/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2014-2019, Regents of the University of California
 *
 * NAC library is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * NAC library is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.
 *
 * You should have received copies of the GNU General Public License and GNU Lesser
 * General Public License along with ndn-cxx, e.g., in COPYING.md file.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * See AUTHORS.md for complete list of NAC library authors and contributors.
 */

#ifndef NDN_NAC_ACCESS_MANAGER_HPP
#define NDN_NAC_ACCESS_MANAGER_HPP

#include "common.hpp"
#include "data-base.hpp"
#include "schedule.hpp"
#include "location-list.hpp"
#include "time-location-list.hpp"

#include <ndn-cxx/face.hpp>
#include <ndn-cxx/security/key-params.hpp>
#include <ndn-cxx/encoding/buffer-stream.hpp>
#include <ndn-cxx/security/transform/private-key.hpp>
#include <ndn-cxx/security/transform/public-key.hpp>

namespace ndn {
namespace nac {

/**
 * @brief Access Manager
 *
 * Access Manager controls decryption policy by publishing granular per-namespace access
 * policies in the form of key encryption (KEK, plaintext public) and key decryption (KDK,
 * encrypted private key) key pair.
 *
 * @todo Rolling KEK
 */
class AccessManager
{
public:
  class Error : public std::runtime_error
  {
  public:
    using std::runtime_error::runtime_error;
  };

public:
  /**
   * @param identity Identity of the namespace (i.e., public and private keys)
   *
   * @param identity Data owner's namespace identity (will be used to sign KEK and KDK)
   * @param dataset Name of dataset that this manager is controlling
   * @param keyChain KeyChain
   * @param face Face that will be used to publish KEK and KDKs
   *
   * KEK and KDK naming:
   *
   *     [identity]/NAC/[dataset]/KEK            /[key-id]                           (== KEK, public key)
   *
   *     [identity]/NAC/[dataset]/KDK/[key-id]   /ENCRYPTED-BY/[user]/KEY/[key-id]   (== KDK, encrypted private key)
   *
   *     \_____________  ______________/
   *                   \/
   *          registered with NFD
   *
   * AccessManager serves NAC public key for data producers to fetch and encrypted versions of
   * private keys (as safe bags) for authorized consumers to fetch.
   */
  AccessManager(const Identity& identity, const Name& dataset,
                KeyChain& keyChain, Face& face);

  AccessManager(const Identity& identity, const Name& dataset,
                KeyChain& keyChain, Face& face,
                const std::string& dbPath, 
                const KeyParams& params, const int freshPeriod);


void
  getKdk();

  std::list<Data>
  getKeklist();
  /**
   * @brief Create a group key for interval which
   *        @p timeslot falls into
   *
   * This method creates a group key if it does not
   * exist, and encrypts the key using public key of
   * all eligible members.
   *
   * @p needRegenerate should be true if 1.first time to call 2.a member was removed
   *                   and it can be false if 1.not the first time to call 2.a member was added
   *
   * @returns The group key (the first one is the
   *          public key, and the rest are encrypted
   *          private key.
   */
  void
  getGroupKey(const TimeStamp& stimeslot, const TimeStamp& etimeslot, const std::vector< std::vector<double>>  locations, bool needRegenerate = true);

  /**
   * @brief Authorize a member identified by its certificate @p memberCert to decrypt data
   *        under the policy
   * @return published KDK
   */
  Data
  addMember(const Certificate& memberCert);

  void
  addMember(const std::string& scheduleName, const Certificate& memberCert);

  void
  addMemberLocation(const std::string& listName, const Certificate& memberCert);

  void
  addMemberTimeLocation(const std::string& listName, const Certificate& memberCert);

  /// @brief Add @p schedule with @p scheduleName
  void
  addSchedule(const std::string& scheduleName, const Schedule& schedule);

  void
  addLocationList(const std::string& listName, const LocationList& locationlist);

  void
  addTimeLocationList(const std::string& listName, const TimeLocationList& locationlist);

  /// @brief Create KEK data.
  Data
  createKekData(const Name& kekName,
                const Certificate& newCert);

  /// @brief Create KDK data.
  Data
  createKdkData(const Name& kdkPrefix,
                const Certificate& newCert,
                const Name& keyName,
                const Buffer& certKey);

  /**
   * @brief Remove member with name @p identity from the group
   */
  void
  removeMember(const Name& identity);

  /**
   * @brief Calculate interval that covers @p timeslot
   * and fill @p memberKeys with the info of members who is allowed to access the interval.
   */
  Interval
  calculateInterval(const TimeStamp& timeslot, std::map<Name, Buffer>& certMap);

  /**
   * @brief Calculate location that covers @p latitude and @p longitude
   * and fill @p memberKeys with the info of members who is allowed to access the location.
   */
  Location
  calculateLocation(const double& latitude, const double& longitude, const double& radius, std::map<Name, Buffer>& memberKeys);

  TimeLocation
  calculateTimeLocation(const TimeStamp& timeslot, const double& latitude, const double& longitude, const double& radius, std::map<Name, Buffer>& memberKeys);

public: // accessor interface for published data packets

  /** @return{ number of packets stored in in-memory storage }
   */
  size_t
  size() const
  {
    return m_ims.size();
  }

  /** @brief Returns begin iterator of the in-memory storage ordered by
   *  name with digest
   *
   *  @return{ const_iterator pointing to the beginning of m_cache }
   */
  InMemoryStorage::const_iterator
  begin() const
  {
    return m_ims.begin();
  }

  /** @brief Returns end iterator of the in-memory storage ordered by
   *  name with digest
   *
   *  @return{ const_iterator pointing to the end of m_cache }
   */
  InMemoryStorage::const_iterator
  end() const
  {
    return m_ims.end();
  }

  const Name&
  getIdentity() const
  {
    return m_identity.getName();
  }

Key m_nacKey;

private:
  Identity m_identity;
  // Key m_nacKey;
  KeyChain& m_keyChain;
  Face& m_face;
  int m_freshPeriod; // Key Freshness Period

  InMemoryStoragePersistent m_ims; // for KEK and KDKs
  ScopedRegisteredPrefixHandle m_kekReg;
  ScopedRegisteredPrefixHandle m_kdkReg;

  TimeStamp m_startTimeSlot;
  TimeStamp m_endTimeSlot;
  bool m_last;
  bool m_found;

  DataBase m_db;
  const KeyParams m_params; // RSA or EC key size

  std::list<Data> m_dataList;
};

} // namespace nac
} // namespace ndn

#endif // NDN_NAC_ACCESS_MANAGER_HPP
