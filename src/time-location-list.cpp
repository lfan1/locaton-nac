/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/**
 * Copyright (c) 2014-2018, Regents of the University of California
 *
 * This file is part of NAC (Name-Based Access Control for NDN).
 * See AUTHORS.md for complete list of NAC authors and contributors.
 *
 * NAC is free software: you can redistribute it and/or modify it under the terms
 * of the GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * NAC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * NAC, e.g., in COPYING.md file.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Zhiyi Zhang <zhiyi@cs.ucla.edu>
 */

#include "time-location-list.hpp"

#include <ndn-cxx/encoding/block-helpers.hpp>
#include <ndn-cxx/util/concepts.hpp>

#include <iostream>

namespace ndn {
namespace nac {

static void
calTimeLocationResult(const TimeStamp& ts, 
                      const std::set<TimeLocation>& list,
                      const double& latitude, 
                      const double& longitude,
                      const double& radius,
                      TimeLocation& positiveR,
                      TimeLocation& negativeR)
{
  TimeLocation tempLocation(false);
  bool isPositive;

  for (const TimeLocation& element : list) {
    std::tie(isPositive, tempLocation) = element.getTimeLocation(ts, latitude, longitude, radius);

    if (isPositive == true) {
      positiveR || tempLocation;
      positiveR =  TimeLocation(latitude, longitude, radius, positiveR.getStartTime(), positiveR.getEndTime());

    }
    else { 
      if (!negativeR.isValid())
        negativeR = TimeLocation(latitude, longitude, radius, tempLocation.getStartTime(), tempLocation.getEndTime());
      else
        negativeR&& TimeLocation(latitude, longitude, radius, tempLocation.getStartTime(), tempLocation.getEndTime());;
    }
  }

}

BOOST_CONCEPT_ASSERT((WireEncodable<TimeLocationList>));
BOOST_CONCEPT_ASSERT((WireDecodable<TimeLocationList>));

TimeLocationList::TimeLocationList() = default;

TimeLocationList::TimeLocationList(const Block& block)
{
  wireDecode(block);
}

template<encoding::Tag TAG>
size_t
TimeLocationList::wireEncode(EncodingImpl<TAG>& encoder) const
{
  size_t totalLength = 0;
  size_t blackLength = 0;
  size_t whiteLength = 0;

  // encode the blackIntervalList as an embed TLV structure
  for (auto it = m_blackTimeLocation.rbegin(); it != m_blackTimeLocation.rend(); it++) {
    blackLength += encoder.prependBlock(it->wireEncode());
  }
  blackLength += encoder.prependVarNumber(blackLength);
  blackLength += encoder.prependVarNumber(tlv::BlackTimeLocationList);

  // encode the whiteIntervalList as an embed TLV structure
  for (auto it = m_whiteTimeLocation.rbegin(); it != m_whiteTimeLocation.rend(); it++) {
    whiteLength += encoder.prependBlock(it->wireEncode());
  }
  whiteLength += encoder.prependVarNumber(whiteLength);
  whiteLength += encoder.prependVarNumber(tlv::WhiteTimeLocationList);

  totalLength = whiteLength + blackLength;
  totalLength += encoder.prependVarNumber(totalLength);
  totalLength += encoder.prependVarNumber(tlv::TimeLocationList);

  return totalLength;
}

const Block&
TimeLocationList::wireEncode() const
{
  if (m_wire.hasWire())
    return m_wire;

  EncodingEstimator estimator;
  size_t estimatedSize = wireEncode(estimator);

  EncodingBuffer buffer(estimatedSize, 0);
  wireEncode(buffer);

  this->m_wire = buffer.block();
  return m_wire;
}

void
TimeLocationList::wireDecode(const Block& wire)
{
  if (wire.type() != tlv::TimeLocationList)
    BOOST_THROW_EXCEPTION(tlv::Error("Unexpected TLV type when decoding TimeLocationList"));

  m_wire = wire;
  m_wire.parse();

  if (m_wire.elements_size() != 2)
    BOOST_THROW_EXCEPTION(tlv::Error("TimeLocationList tlv does not have two sub-TLVs"));

  Block::element_const_iterator it = m_wire.elements_begin();

  if (it != m_wire.elements_end() && it->type() == tlv::WhiteTimeLocationList) {
    it->parse();
    Block::element_const_iterator tempIt = it->elements_begin();
    while (tempIt != it->elements_end() && tempIt->type() == tlv::TimeLocation) {
      m_whiteTimeLocation.insert(TimeLocation(*tempIt));
      tempIt++;
    }
    it++;
  }
  else
    BOOST_THROW_EXCEPTION(tlv::Error("The first element must be WhiteTimeLocationList"));

  if (it != m_wire.elements_end() && it->type() == tlv::BlackTimeLocationList) {
    it->parse();
    Block::element_const_iterator tempIt = it->elements_begin();
    while (tempIt != it->elements_end() && tempIt->type() == tlv::TimeLocation) {
      m_blackTimeLocation.insert(TimeLocation(*tempIt));
      tempIt++;
    }
    it++;
  }
  else
    BOOST_THROW_EXCEPTION(tlv::Error("The second element must be BlackTimeLocationList"));
}

TimeLocationList&
TimeLocationList::addWhiteTimeLocation(const TimeLocation& timeLocation)
{
  m_wire.reset();
  m_whiteTimeLocation.insert(timeLocation);
  return *this;
}

TimeLocationList&
TimeLocationList::addBlackTimeLocation(const TimeLocation& timeLocation)
{
  m_wire.reset();
  m_blackTimeLocation.insert(timeLocation);
  return *this;
}

std::tuple<bool, TimeLocation>
TimeLocationList::getCoveringTimeLocation(const TimeStamp& ts, const double& latitude, const double& longitude, const double& radius) const
{
  TimeLocation blackPositiveResult(true);
  TimeLocation whitePositiveResult(true);

  TimeLocation blackNegativeResult(false);
  TimeLocation whiteNegativeResult(false);

  // get the blackResult
  calTimeLocationResult(ts, m_blackTimeLocation, latitude, longitude, radius, blackPositiveResult, blackNegativeResult);

  // if black positive result is not empty, the result must be false
  if (!blackPositiveResult.isEmpty())
    return std::make_tuple(false, blackPositiveResult);

  // get the whiteResult
  calTimeLocationResult(ts, m_whiteTimeLocation, latitude, longitude, radius, whitePositiveResult, whiteNegativeResult);

  if (!whitePositiveResult.isEmpty()) {
    // there is white interval covering the timestamp
    // return ture 
      return std::make_tuple(true, whitePositiveResult);
  }
  else {
    // there is no white interval covering the timestamp
    // return false
    return std::make_tuple(false, whiteNegativeResult);
  }
}

} // namespace nac
} // namespace ndn
