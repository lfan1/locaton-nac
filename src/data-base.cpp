/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/**
 * Copyright (c) 2014-2018, Regents of the University of California
 *
 * This file is part of NAC (Name-Based Access Control for NDN).
 * See AUTHORS.md for complete list of NAC authors and contributors.
 *
 * NAC is free software: you can redistribute it and/or modify it under the terms
 * of the GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * NAC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * NAC, e.g., in COPYING.md file.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Prashanth Swaminathan <prashanthsw@gmail.com>
 */

#include "data-base.hpp"
#include <ndn-cxx/util/sqlite3-statement.hpp>
#include <iostream>
#include <boost/filesystem.hpp>
#include <sqlite3.h>

namespace ndn {
namespace nac {

using util::Sqlite3Statement;
using time::system_clock;

static const std::string INITIALIZATION = R"_DBTEXT_(
CREATE TABLE IF NOT EXISTS
  contentkeys(
    rowId            INTEGER PRIMARY KEY,
    timeslot         INTEGER,
    key              BLOB NOT NULL
  );
CREATE UNIQUE INDEX IF NOT EXISTS
   timeslotIndex ON contentkeys(timeslot);

CREATE TABLE IF NOT EXISTS
  schedules(
    schedule_id         INTEGER PRIMARY KEY,
    schedule_name       TEXT NOT NULL,
    schedule            BLOB NOT NULL
  );
CREATE UNIQUE INDEX IF NOT EXISTS
   scheduleNameIndex ON schedules(schedule_name);

CREATE TABLE IF NOT EXISTS
  contentkeysL(
    rowId            INTEGER PRIMARY KEY,
    latitude         TEXT NOT NULL,
    longitude        TEXT NOT NULL,
    key              BLOB NOT NULL
  );
CREATE UNIQUE INDEX IF NOT EXISTS
   locationIndex ON contentkeysL(latitude, longitude);

CREATE TABLE IF NOT EXISTS
  contentkeysTL(
    rowId            INTEGER PRIMARY KEY,
    keyname          BLOB NOT NULL,
    key              BLOB NOT NULL
  );
CREATE UNIQUE INDEX IF NOT EXISTS
   timelocationIndex ON contentkeysTL(keyname);

CREATE TABLE IF NOT EXISTS
  locations(
    list_id         INTEGER PRIMARY KEY,
    list_name       TEXT NOT NULL,
    list_type       TEXT NOT NULL,
    list            BLOB NOT NULL
  );

CREATE TABLE IF NOT EXISTS
  members(
    member_id           INTEGER PRIMARY KEY,
    schedule_id         INTEGER NOT NULL,
    member_name         BLOB NOT NULL,
    key_name            BLOB NOT NULL,
    pubkey              BLOB NOT NULL,
    FOREIGN KEY(schedule_id)
      REFERENCES schedules(schedule_id)
      ON DELETE CASCADE
      ON UPDATE CASCADE
  );


CREATE TABLE IF NOT EXISTS
  membersL(
    member_id           INTEGER PRIMARY KEY,
    list_id             INTEGER NOT NULL,
    member_name         BLOB NOT NULL,
    key_name            BLOB NOT NULL,
    pubkey              BLOB NOT NULL,
    FOREIGN KEY(list_id)
      REFERENCES locations(list_id)
      ON DELETE CASCADE
      ON UPDATE CASCADE
  );


CREATE TABLE IF NOT EXISTS
  ekeys(
    ekey_id             INTEGER PRIMARY KEY,
    ekey_name           BLOB NOT NULL,
    pub_key             BLOB NOT NULL
  );
CREATE UNIQUE INDEX IF NOT EXISTS
   ekeyNameIndex ON ekeys(ekey_name);)_DBTEXT_";

class DataBase::Impl
{
public:
  Impl(const std::string& dbPath)
  {
    // open Database

    int result = sqlite3_open_v2(dbPath.c_str(),
                                 &m_database,
                                 SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE,
                                 nullptr);

    if (result != SQLITE_OK)
      BOOST_THROW_EXCEPTION(Error("Key storage cannot be opened/created: " + dbPath));

    // enable foreign key
    sqlite3_exec(m_database, "PRAGMA foreign_keys = ON", nullptr, nullptr, nullptr);

    // initialize database specific tables
    char* errorMessage = nullptr;
    result = sqlite3_exec(m_database, INITIALIZATION.c_str(), nullptr, nullptr, &errorMessage);
    if (result != SQLITE_OK && errorMessage != nullptr) {
      sqlite3_free(errorMessage);
      BOOST_THROW_EXCEPTION(Error("Key storage cannot be initialized"));
    }
  }

  ~Impl()
  {
    sqlite3_close(m_database);
  }

  int
  getScheduleId(const std::string& name) const
  {
    Sqlite3Statement statement(m_database,
                               R"_DBTEXT_(SELECT schedule_id FROM schedules
                               WHERE schedule_name=?)_DBTEXT_");
    statement.bind(1, name, SQLITE_TRANSIENT);

    int result = -1;
    if (statement.step() == SQLITE_ROW)
      result = statement.getInt(0);
    return result;
  }

  int
  getLocationId(const std::string& name) const
  {
    Sqlite3Statement statement(m_database,
                               R"_DBTEXT_(SELECT list_id FROM locations
                               WHERE list_name=?)_DBTEXT_");
    statement.bind(1, name, SQLITE_TRANSIENT);

    int result = -1;
    if (statement.step() == SQLITE_ROW)
      result = statement.getInt(0);
    return result;
  }

public:
  sqlite3* m_database;
  std::map<Name, Buffer> m_priKeyBase;
};

DataBase::DataBase(const std::string& dbPath)
  : m_impl(new Impl(dbPath))
{
}

DataBase::~DataBase() = default;

static int32_t
getFixedTimeslot(const system_clock::TimePoint& timeslot)
{
  return (time::toUnixTimestamp(timeslot)).count() / 60000;
}

bool
DataBase::hasContentKey(const system_clock::TimePoint& timeslot) const
{
  int32_t fixedTimeslot = getFixedTimeslot(timeslot);
  Sqlite3Statement statement(m_impl->m_database,
                             R"_DBTEXT_(SELECT key FROM contentkeys where timeslot=?)_DBTEXT_");
  statement.bind(1, fixedTimeslot);
  return (statement.step() == SQLITE_ROW);
}


Buffer
DataBase::getContentKey(const system_clock::TimePoint& timeslot) const
{
  int32_t fixedTimeslot = getFixedTimeslot(timeslot);
  Sqlite3Statement statement(m_impl->m_database,
                             R"_DBTEXT_(SELECT key FROM contentkeys where timeslot=?)_DBTEXT_");
  statement.bind(1, fixedTimeslot);

  Buffer result;
  if (statement.step() == SQLITE_ROW) {
    result = Buffer(statement.getBlob(0), statement.getSize(0));
  }
  else {
    BOOST_THROW_EXCEPTION(Error("Cannot get the key from database"));
  }
  return result;
}

void
DataBase::addContentKey(const system_clock::TimePoint& timeslot, const Buffer& key)
{
  // BOOST_ASSERT(key.length() != 0);
  int32_t fixedTimeslot = getFixedTimeslot(timeslot);
  Sqlite3Statement statement(m_impl->m_database,
                             R"_DBTEXT_(INSERT INTO contentkeys (timeslot, key)
                             values (?, ?))_DBTEXT_");
  statement.bind(1, fixedTimeslot);
  statement.bind(2, key.data(), key.size(), SQLITE_TRANSIENT);
  if (statement.step() != SQLITE_DONE) {
    BOOST_THROW_EXCEPTION(Error("Cannot add the key to database"));
  }
}

void
DataBase::deleteContentKey(const system_clock::TimePoint& timeslot)
{
  int32_t fixedTimeslot = getFixedTimeslot(timeslot);
  Sqlite3Statement statement(m_impl->m_database,
                             R"_DBTEXT_(DELETE FROM contentkeys WHERE timeslot=?)_DBTEXT_");
  statement.bind(1, fixedTimeslot);
  statement.step();
}

bool
DataBase::hasSchedule(const std::string& name) const
{
  Sqlite3Statement statement(m_impl->m_database,
                             R"_DBTEXT_(SELECT schedule_id FROM schedules
                             WHERE schedule_name=?)_DBTEXT_");
  statement.bind(1, name, SQLITE_TRANSIENT);
  return (statement.step() == SQLITE_ROW);
}

std::list<std::string>
DataBase::listAllScheduleNames() const
{
  std::list<std::string> result;
  Sqlite3Statement statement(m_impl->m_database,
                             R"_DBTEXT_(SELECT schedule_name FROM schedules)_DBTEXT_");

  result.clear();
  while (statement.step() == SQLITE_ROW) {
    result.push_back(statement.getString(0));
  }
  return result;
}

Schedule
DataBase::getSchedule(const std::string& name) const
{
  Sqlite3Statement statement(m_impl->m_database,
                             R"_DBTEXT_(SELECT schedule FROM schedules where schedule_name=?)_DBTEXT_");
  statement.bind(1, name, SQLITE_TRANSIENT);

  Schedule result;
  if (statement.step() == SQLITE_ROW) {
    result.wireDecode(statement.getBlock(0));
  }
  else {
    BOOST_THROW_EXCEPTION(Error("Cannot get the result from database"));
  }
  return result;
}

std::map<Name, Buffer>
DataBase::getScheduleMembers(const std::string& name) const
{
  std::map<Name, Buffer> result;
  Sqlite3Statement statement(m_impl->m_database,
                             R"_DBTEXT_(SELECT key_name, pubkey
                             FROM members JOIN schedules
                             ON members.schedule_id=schedules.schedule_id
                             WHERE schedule_name=?)_DBTEXT_");
  statement.bind(1, name, SQLITE_TRANSIENT);
  result.clear();

  const uint8_t* keyBytes = nullptr; 
  while (statement.step() == SQLITE_ROW) {
    keyBytes = statement.getBlob(1);
    const int& keyBytesSize = statement.getSize(1);
    result.insert(std::pair<Name, Buffer>(Name(statement.getBlock(0)),
                                          Buffer(keyBytes, keyBytesSize)));
  }
  return result;
}

std::map<Name, Buffer>
DataBase::getScheduleMembersL(const std::string& name) const
{
  std::map<Name, Buffer> result;
  Sqlite3Statement statement(m_impl->m_database,
                             R"_DBTEXT_(SELECT key_name, pubkey
                             FROM membersL JOIN locations
                             ON membersL.list_id=locations.list_id
                             WHERE list_name=?)_DBTEXT_");
  statement.bind(1, name, SQLITE_TRANSIENT);
  result.clear();

  const uint8_t* keyBytes = nullptr;
  while (statement.step() == SQLITE_ROW) {
    keyBytes = statement.getBlob(1);
    const int& keyBytesSize = statement.getSize(1);
    result.insert(std::pair<Name, Buffer>(Name(statement.getBlock(0)),
                                          Buffer(keyBytes, keyBytesSize)));
  }
  return result;
}

void
DataBase::addSchedule(const std::string& name, const Schedule& schedule)
{
  BOOST_ASSERT(name.length() != 0);

  Sqlite3Statement statement(m_impl->m_database,
                             R"_DBTEXT_(INSERT INTO schedules (schedule_name, schedule)
                             values (?, ?))_DBTEXT_");
  statement.bind(1, name, SQLITE_TRANSIENT);
  statement.bind(2, schedule.wireEncode(), SQLITE_TRANSIENT);
  if (statement.step() != SQLITE_DONE)
    BOOST_THROW_EXCEPTION(Error("Cannot add the schedule to database"));
}

void
DataBase::deleteSchedule(const std::string& name)
{
  Sqlite3Statement statement(m_impl->m_database,
                             R"_DBTEXT_(DELETE FROM schedules WHERE schedule_name=?)_DBTEXT_");
  statement.bind(1, name, SQLITE_TRANSIENT);
  statement.step();
}

void
DataBase::renameSchedule(const std::string& oldName, const std::string& newName)
{
  BOOST_ASSERT(newName.length() != 0);

  Sqlite3Statement statement(m_impl->m_database,
                             R"_DBTEXT_(UPDATE schedules SET schedule_name=?
                             WHERE schedule_name=?)_DBTEXT_");
  statement.bind(1, newName, SQLITE_TRANSIENT);
  statement.bind(2, oldName, SQLITE_TRANSIENT);
  if (statement.step() != SQLITE_DONE)
    BOOST_THROW_EXCEPTION(Error("Cannot rename the schedule from database"));
}

void
DataBase::updateSchedule(const std::string& name, const Schedule& schedule)
{
  if (!hasSchedule(name)) {
    addSchedule(name, schedule);
    return;
  }

  Sqlite3Statement statement(m_impl->m_database,
                             R"_DBTEXT_(UPDATE schedules SET schedule=?
                             WHERE schedule_name=?)_DBTEXT_");
  statement.bind(1, schedule.wireEncode(), SQLITE_TRANSIENT);
  statement.bind(2, name, SQLITE_TRANSIENT);
  statement.step();
}

//location related content key

bool
DataBase::hasContentKey(const double& latitude, const double& longitude) const
{
  // int32_t fixedTimeslot = getFixedTimeslot(timeslot);
  Sqlite3Statement statement(m_impl->m_database,
                             R"_DBTEXT_(SELECT key FROM contentkeysL where latitude=? and longitude=?)_DBTEXT_");
  statement.bind(1, std::to_string(latitude), SQLITE_TRANSIENT);
  statement.bind(2, std::to_string(longitude), SQLITE_TRANSIENT);
  
  return (statement.step() == SQLITE_ROW);
}


Buffer
DataBase::getContentKey(const double& latitude, const double& longitude) const
{
  // int32_t fixedTimeslot = getFixedTimeslot(timeslot);
  Sqlite3Statement statement(m_impl->m_database,
                             R"_DBTEXT_(SELECT key FROM contentkeysL where latitude=? and longitude=?)_DBTEXT_");
  statement.bind(1, std::to_string(latitude), SQLITE_TRANSIENT);
  statement.bind(2, std::to_string(longitude), SQLITE_TRANSIENT);

  Buffer result;
  if (statement.step() == SQLITE_ROW) {
    result = Buffer(statement.getBlob(0), statement.getSize(0));
  }
  else {
    BOOST_THROW_EXCEPTION(Error("Cannot get the key from database"));
  }
  return result;
}

void
DataBase::addContentKey(const double& latitude, const double& longitude, const Buffer& key)
{
  // BOOST_ASSERT(key.length() != 0);
  // int32_t fixedTimeslot = getFixedTimeslot(timeslot);
  Sqlite3Statement statement(m_impl->m_database,
                             R"_DBTEXT_(INSERT INTO contentkeysL (latitude, longitude, key)
                             values (?, ?, ?))_DBTEXT_");
  statement.bind(1, std::to_string(latitude), SQLITE_TRANSIENT);
  statement.bind(2, std::to_string(longitude), SQLITE_TRANSIENT);
  statement.bind(3, key.data(), key.size(), SQLITE_TRANSIENT);
  if (statement.step() != SQLITE_DONE) {
    BOOST_THROW_EXCEPTION(Error("Cannot add the key to database"));
  }
}

void
DataBase::deleteContentKey(const double& latitude, const double& longitude)
{
  // int32_t fixedTimeslot = getFixedTimeslot(timeslot);
  Sqlite3Statement statement(m_impl->m_database,
                             R"_DBTEXT_(DELETE FROM contentkeysL WHERE latitude=? and longitude=?)_DBTEXT_");
  statement.bind(1, std::to_string(latitude), SQLITE_TRANSIENT);
  statement.bind(2, std::to_string(longitude), SQLITE_TRANSIENT);
  statement.step();
}

//time-location related content key 

bool
DataBase::hasContentKey(const Name& keyName) const
{
  // int32_t fixedTimeslot = getFixedTimeslot(timeslot);
  Sqlite3Statement statement(m_impl->m_database,
                             R"_DBTEXT_(SELECT key FROM contentkeysTL where keyname=?)_DBTEXT_");
  // statement.bind(1, fixedTimeslot);
  // statement.bind(2, std::to_string(latitude), SQLITE_TRANSIENT);
  // statement.bind(3, std::to_string(longitude), SQLITE_TRANSIENT);
  // statement.bind(4, std::to_string(radius), SQLITE_TRANSIENT);
  statement.bind(1, keyName.wireEncode(), SQLITE_TRANSIENT);
  
  return (statement.step() == SQLITE_ROW);
}


Buffer
DataBase::getContentKey(const Name& keyName) const
{
  // int32_t fixedTimeslot = getFixedTimeslot(timeslot);
  Sqlite3Statement statement(m_impl->m_database,
                             R"_DBTEXT_(SELECT key FROM contentkeysTL where keyname=?)_DBTEXT_");
  // statement.bind(1, fixedTimeslot);
  // statement.bind(2, std::to_string(latitude), SQLITE_TRANSIENT);
  // statement.bind(3, std::to_string(longitude), SQLITE_TRANSIENT);
  // statement.bind(4, std::to_string(radius), SQLITE_TRANSIENT);
  statement.bind(1, keyName.wireEncode(), SQLITE_TRANSIENT);

  Buffer result;
  if (statement.step() == SQLITE_ROW) {
    result = Buffer(statement.getBlob(0), statement.getSize(0));
  }
  else {
    BOOST_THROW_EXCEPTION(Error("Cannot get the key from database"));
  }
  return result;
}

void
DataBase::addContentKey(const Name& keyName, const Buffer& key)
{
  // BOOST_ASSERT(key.length() != 0);
  // int32_t fixedTimeslot = getFixedTimeslot(timeslot);
  Sqlite3Statement statement(m_impl->m_database,
                             R"_DBTEXT_(INSERT INTO contentkeysTL (keyname, key)
                             values (?, ?))_DBTEXT_");
  // statement.bind(1, fixedTimeslot);
  // statement.bind(2, std::to_string(latitude), SQLITE_TRANSIENT);
  // statement.bind(3, std::to_string(longitude), SQLITE_TRANSIENT);
  // statement.bind(4, std::to_string(radius), SQLITE_TRANSIENT);
  statement.bind(1, keyName.wireEncode(), SQLITE_TRANSIENT);
  statement.bind(2, key.data(), key.size(), SQLITE_TRANSIENT);

  if (statement.step() != SQLITE_DONE) {
    BOOST_THROW_EXCEPTION(Error("Cannot add the content key to database"));
  }
}

void
DataBase::deleteContentKey(const Name& keyName)
{
  // int32_t fixedTimeslot = getFixedTimeslot(timeslot);
  Sqlite3Statement statement(m_impl->m_database,
                             R"_DBTEXT_(DELETE FROM contentkeysTL WHERE keyname=?)_DBTEXT_");
  // statement.bind(1, fixedTimeslot);
  // statement.bind(2, std::to_string(latitude), SQLITE_TRANSIENT);
  // statement.bind(3, std::to_string(longitude), SQLITE_TRANSIENT);
  // statement.bind(4, std::to_string(radius), SQLITE_TRANSIENT);

  statement.bind(1, keyName.wireEncode(), SQLITE_TRANSIENT);
  statement.step();
}

bool
DataBase::hasLocationList(const std::string& name) const
{
  Sqlite3Statement statement(m_impl->m_database,
                             R"_DBTEXT_(SELECT list_id FROM locations
                             WHERE list_name=?)_DBTEXT_");
  statement.bind(1, name, SQLITE_TRANSIENT);
  return (statement.step() == SQLITE_ROW);
}

std::list<std::string>
DataBase::listAllLocationListNames(const std::string& type) const
{
  std::list<std::string> result;
  Sqlite3Statement statement(m_impl->m_database,
                             R"_DBTEXT_(SELECT list_name FROM locations where list_type=?)_DBTEXT_");

  statement.bind(1, type, SQLITE_TRANSIENT);
  result.clear();
  while (statement.step() == SQLITE_ROW) {
    result.push_back(statement.getString(0));
  }
  return result;
}

LocationList
DataBase::getLocationList(const std::string& name) const
{
  Sqlite3Statement statement(m_impl->m_database,
                             R"_DBTEXT_(SELECT list FROM locations where list_name=?)_DBTEXT_");
  statement.bind(1, name, SQLITE_TRANSIENT);
  statement.bind(2, "LocationList", SQLITE_TRANSIENT);

  LocationList result;

  if (statement.step() == SQLITE_ROW) {
    result.wireDecode(statement.getBlock(0));
  }
  else {
  BOOST_THROW_EXCEPTION(Error("Cannot get the result from database"));
  }

  return result;
}

TimeLocationList
DataBase::getTimeLocationList(const std::string& name) const
{
  Sqlite3Statement statement(m_impl->m_database,
                             R"_DBTEXT_(SELECT list FROM locations where list_name=? and list_type=?)_DBTEXT_");
  statement.bind(1, name, SQLITE_TRANSIENT);
  statement.bind(2, "TimeLocationList", SQLITE_TRANSIENT);
  
  TimeLocationList result;

  if (statement.step() == SQLITE_ROW) {

    result.wireDecode(statement.getBlock(0));
  }
  else {
    // return result;
    BOOST_THROW_EXCEPTION(Error("Cannot get the result from database"));
  }
  
  return result;
}

std::map<Name, Buffer>
DataBase::getLocationListMembers(const std::string& name) const
{
  std::map<Name, Buffer> result;
  Sqlite3Statement statement(m_impl->m_database,
                             R"_DBTEXT_(SELECT key_name, pubkey
                             FROM membersL JOIN locations
                             ON membersL.list_id=locations.list_id
                             WHERE list_name=?)_DBTEXT_");
  statement.bind(1, name, SQLITE_TRANSIENT);
  result.clear();

  const uint8_t* keyBytes = nullptr;
  while (statement.step() == SQLITE_ROW) {
    keyBytes = statement.getBlob(1);
    const int& keyBytesSize = statement.getSize(1);
    result.insert(std::pair<Name, Buffer>(Name(statement.getBlock(0)),
                                          Buffer(keyBytes, keyBytesSize)));
  }
  return result;
}

void
DataBase::addLocationList(const std::string& name, const LocationList& locationList)
{
  BOOST_ASSERT(name.length() != 0);

  Sqlite3Statement statement(m_impl->m_database,
                             R"_DBTEXT_(INSERT INTO locations (list_name, list_type, list)
                             values (?, ?, ?))_DBTEXT_");
  statement.bind(1, name, SQLITE_TRANSIENT);
  statement.bind(2, "LocationList",SQLITE_TRANSIENT);
  statement.bind(3, locationList.wireEncode(), SQLITE_TRANSIENT);
  if (statement.step() != SQLITE_DONE)
    BOOST_THROW_EXCEPTION(Error("Cannot add the schedule to database"));
}

void
DataBase::deleteLocationList(const std::string& name)
{
  Sqlite3Statement statement(m_impl->m_database,
                             R"_DBTEXT_(DELETE FROM locations WHERE list_name=?)_DBTEXT_");
  statement.bind(1, name, SQLITE_TRANSIENT);
  statement.step();
}

void
DataBase::renameLocationList(const std::string& oldName, const std::string& newName)
{
  BOOST_ASSERT(newName.length() != 0);

  Sqlite3Statement statement(m_impl->m_database,
                             R"_DBTEXT_(UPDATE locations SET list_name=?
                             WHERE list_name=?)_DBTEXT_");
  statement.bind(1, newName, SQLITE_TRANSIENT);
  statement.bind(2, oldName, SQLITE_TRANSIENT);
  if (statement.step() != SQLITE_DONE)
    BOOST_THROW_EXCEPTION(Error("Cannot rename the schedule from database"));
}

void
DataBase::updateLocationList(const std::string& name, const LocationList& locationList)
{
  if (!hasLocationList(name)) {
    addLocationList(name, locationList);
    return;
  }

  Sqlite3Statement statement(m_impl->m_database,
                             R"_DBTEXT_(UPDATE locations SET list=?
                             WHERE list_name=?)_DBTEXT_");
  statement.bind(1, locationList.wireEncode(), SQLITE_TRANSIENT);
  statement.bind(2, name, SQLITE_TRANSIENT);
  statement.step();
}
// locationlist

void
DataBase::addTimeLocationList(const std::string& name, const TimeLocationList& locationList)
{
  BOOST_ASSERT(name.length() != 0);

  Sqlite3Statement statement(m_impl->m_database,
                             R"_DBTEXT_(INSERT INTO locations (list_name, list_type, list)
                             values (?, ?, ?))_DBTEXT_");
  statement.bind(1, name, SQLITE_TRANSIENT);
  statement.bind(2, "TimeLocationList",SQLITE_TRANSIENT);
  statement.bind(3, locationList.wireEncode(), SQLITE_TRANSIENT);

  std::cout <<"addTimeLocationList:" << name<< std::endl;
  if (statement.step() != SQLITE_DONE)
    BOOST_THROW_EXCEPTION(Error("Cannot add the schedule to database"));
}


bool
DataBase::hasMember(const Name& identity) const
{
  Sqlite3Statement statement(m_impl->m_database,
                             R"_DBTEXT_(SELECT member_id FROM members WHERE member_name=?)_DBTEXT_");
  statement.bind(1, identity.wireEncode(), SQLITE_TRANSIENT);
  return (statement.step() == SQLITE_ROW);
}

std::list<Name>
DataBase::listAllMembers() const
{
  std::list<Name> result;
  Sqlite3Statement statement(m_impl->m_database,
                             R"_DBTEXT_(SELECT member_name FROM members)_DBTEXT_");

  result.clear();
  while (statement.step() == SQLITE_ROW) {
    result.push_back(Name(statement.getBlock(0)));
  }
  return result;
}

std::string
DataBase::getMemberSchedule(const Name& identity) const
{
  Sqlite3Statement statement(m_impl->m_database,
                             R"_DBTEXT_(SELECT schedule_name
                             FROM schedules JOIN members
                             ON schedules.schedule_id = members.schedule_id
                             WHERE member_name=?)_DBTEXT_");
  statement.bind(1, identity.wireEncode(), SQLITE_TRANSIENT);

  std::string result = "";
  if (statement.step() == SQLITE_ROW) {
    result = statement.getString(0);
  }
  else {
    BOOST_THROW_EXCEPTION(Error("Cannot get the result from database"));
  }
  return result;
}

void
DataBase::addMember(const std::string& scheduleName, const Name& keyName, const Buffer& key)
{
  int scheduleId = m_impl->getScheduleId(scheduleName);
  if (scheduleId == -1)
    BOOST_THROW_EXCEPTION(Error("The schedule dose not exist"));

  // need to be changed in the future
  Name memberName = keyName.getPrefix(-1);

  Sqlite3Statement statement(m_impl->m_database,
                             R"_DBTEXT_(INSERT INTO members (schedule_id, member_name, key_name, pubkey)
                             values (?, ?, ?, ?))_DBTEXT_");
  statement.bind(1, scheduleId);
  statement.bind(2, memberName.wireEncode(), SQLITE_TRANSIENT);
  statement.bind(3, keyName.wireEncode(), SQLITE_TRANSIENT);
  statement.bind(4, key.data(), key.size(), SQLITE_TRANSIENT);

  if (statement.step() != SQLITE_DONE)
    BOOST_THROW_EXCEPTION(Error("Cannot add the member with schedule to database"));
}

bool
DataBase::hasMemberLocation(const Name& identity) const
{
  Sqlite3Statement statement(m_impl->m_database,
                             R"_DBTEXT_(SELECT member_id FROM membersL WHERE member_name=?)_DBTEXT_");
  statement.bind(1, identity.wireEncode(), SQLITE_TRANSIENT);
  return (statement.step() == SQLITE_ROW);
}

void
DataBase::addMemberLocation(const std::string& listName, const Name& keyName, const Buffer& key)
{
  int listId = m_impl->getLocationId(listName);
  if (listId == -1)
    BOOST_THROW_EXCEPTION(Error("The schedule dose not exist"));

  // need to be changed in the future
  Name memberName = keyName.getPrefix(-1);
 
  Sqlite3Statement statement(m_impl->m_database,
                             R"_DBTEXT_(INSERT INTO membersL(list_id, member_name, key_name, pubkey)
                             values (?, ?, ?, ?))_DBTEXT_");
  statement.bind(1, listId);
  statement.bind(2, memberName.wireEncode(), SQLITE_TRANSIENT);
  statement.bind(3, keyName.wireEncode(), SQLITE_TRANSIENT);
  statement.bind(4, key.data(), key.size(), SQLITE_TRANSIENT);

  if (statement.step() != SQLITE_DONE)
    BOOST_THROW_EXCEPTION(Error("Cannot add the member with locations to database"));
}

void
DataBase::addMemberTimeLocation(const std::string& listName, const Name& keyName, const Buffer& key)
{
  int listId = m_impl->getLocationId(listName);
  if (listId == -1)
    BOOST_THROW_EXCEPTION(Error("The schedule dose not exist"));

  // need to be changed in the future
  Name memberName = keyName.getPrefix(-1);
 
  Sqlite3Statement statement(m_impl->m_database,
                             R"_DBTEXT_(INSERT INTO membersL(list_id, member_name, key_name, pubkey)
                             values (?, ?, ?, ?))_DBTEXT_");
  statement.bind(1, listId);
  statement.bind(2, memberName.wireEncode(), SQLITE_TRANSIENT);
  statement.bind(3, keyName.wireEncode(), SQLITE_TRANSIENT);
  statement.bind(4, key.data(), key.size(), SQLITE_TRANSIENT);

  if (statement.step() != SQLITE_DONE)
    BOOST_THROW_EXCEPTION(Error("Cannot add the member with locations to database"));
}

void
DataBase::updateMemberSchedule(const Name& identity, const std::string& scheduleName)
{
  int scheduleId = m_impl->getScheduleId(scheduleName);
  if (scheduleId == -1)
    BOOST_THROW_EXCEPTION(Error("The schedule dose not exist"));

  Sqlite3Statement statement(m_impl->m_database,
                             R"_DBTEXT_(UPDATE members SET schedule_id=?
                             WHERE member_name=?)_DBTEXT_");
  statement.bind(1, scheduleId);
  statement.bind(2, identity.wireEncode(), SQLITE_TRANSIENT);
  statement.step();
}

void
DataBase::deleteMember(const Name& identity)
{
  Sqlite3Statement statement(m_impl->m_database,
                             R"_DBTEXT_(DELETE FROM members WHERE member_name=?)_DBTEXT_");
  statement.bind(1, identity.wireEncode(), SQLITE_TRANSIENT);
  statement.step();
}

bool
DataBase::hasEKey(const Name& eKeyName)
{
  Sqlite3Statement statement(m_impl->m_database,
                             R"_DBTEXT_(SELECT ekey_id FROM ekeys where ekey_name=?)_DBTEXT_");
  statement.bind(1, eKeyName.wireEncode(), SQLITE_TRANSIENT);
  return (statement.step() == SQLITE_ROW);
}

// bool
// DataBase::hasKekCert(const Name& eKeyName)
// {
//   Sqlite3Statement statement(m_impl->m_database,
//                              R"_DBTEXT_(SELECT ekey_id FROM ekeys where ekey_name=?)_DBTEXT_");
//   statement.bind(1, eKeyName.wireEncode(), SQLITE_TRANSIENT);
//   return (statement.step() == SQLITE_ROW);
// }

void
DataBase::addEKey(const Name& eKeyName, const Buffer& pubKey)
{
  Sqlite3Statement statement(m_impl->m_database,
                             R"_DBTEXT_(INSERT INTO ekeys(ekey_name, pub_key)
                             values (?, ?))_DBTEXT_");
  statement.bind(1, eKeyName.wireEncode(), SQLITE_TRANSIENT);
  statement.bind(2, pubKey.data(), pubKey.size(), SQLITE_TRANSIENT);
  if (statement.step() != SQLITE_DONE)
    BOOST_THROW_EXCEPTION(Error("Cannot add the EKey to database"));

  // m_impl->m_priKeyBase[eKeyName] = priKey;
}

void
DataBase::addKekCert(const Name& eKeyName, const Certificate& newCert)
{
  Sqlite3Statement statement(m_impl->m_database,
                             R"_DBTEXT_(INSERT INTO ekeys(ekey_name, cert)
                             values (?, ?))_DBTEXT_");
  statement.bind(1, eKeyName.wireEncode(), SQLITE_TRANSIENT);
  statement.bind(2, newCert.getPublicKey().data(), newCert.getPublicKey().size(), SQLITE_TRANSIENT);
  if (statement.step() != SQLITE_DONE)
    BOOST_THROW_EXCEPTION(Error("Cannot add the ECert to database"));

  // m_impl->m_priKeyBase[eKeyName] = priKey;
}

Buffer
DataBase::getEKey(const Name& eKeyName)
{
  Sqlite3Statement statement(m_impl->m_database,
                             R"_DBTEXT_(SELECT * FROM ekeys where ekey_name=?)_DBTEXT_");
  statement.bind(1, eKeyName.wireEncode(), SQLITE_TRANSIENT);

  Buffer pubKey;
  if (statement.step() == SQLITE_ROW) {
    pubKey = Buffer(statement.getBlob(2), statement.getSize(2));
  }
  else {
    BOOST_THROW_EXCEPTION(Error("Cannot get the result from database"));
  }
  return pubKey;
}

void
DataBase::cleanEKeys()
{
  Sqlite3Statement statement(m_impl->m_database,
                             R"_DBTEXT_(DELETE FROM ekeys)_DBTEXT_");
  statement.step();
  m_impl->m_priKeyBase.clear();
}

void
DataBase::deleteEKey(const Name& eKeyName)
{
  Sqlite3Statement statement(m_impl->m_database,
                             R"_DBTEXT_(DELETE FROM ekeys WHERE ekey_name=?)_DBTEXT_");
  statement.bind(1, eKeyName.wireEncode(), SQLITE_TRANSIENT);
  statement.step();

  auto search = m_impl->m_priKeyBase.find(eKeyName);
  if (search != m_impl->m_priKeyBase.end()) {
    m_impl->m_priKeyBase.erase(search);
  }
}

} // namespace nac
} // namespace ndn
