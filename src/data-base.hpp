/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/**
 * Copyright (c) 2014-2018, Regents of the University of California
 *
 * This file is part of NAC (Name-Based Access Control for NDN).
 * See AUTHORS.md for complete list of NAC authors and contributors.
 *
 * NAC is free software: you can redistribute it and/or modify it under the terms
 * of the GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * NAC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * NAC, e.g., in COPYING.md file.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Prashanth Swaminathan <prashanthsw@gmail.com>
 */

#ifndef NDN_NAC_PRODUCER_DB_HPP
#define NDN_NAC_PRODUCER_DB_HPP

#include "schedule.hpp"
#include "location-list.hpp"
#include "time-location-list.hpp"
#include "common.hpp"

namespace ndn {
namespace nac {

/**
 * @brief DataBase is a class to manage the database of data producer and group manager.
 * It contains three tables that store Content key, Schedules and Members
 */
class DataBase
{
public:
  class Error : public std::runtime_error
  {
  public:
    using std::runtime_error::runtime_error;
  };

public:
  explicit
  DataBase(const std::string& dbPath);

  ~DataBase();

public:
  ////////////////////////////////////////////////////// content key management for time schedule

  /**
   * @brief Check if content key exists for the hour covering @p timeslot
   */
  bool
  hasContentKey(const time::system_clock::TimePoint& timeslot) const;

  /**
   * @brief Get content key for the hour covering @p timeslot
   * @throws Error if the key does not exist
   */
  Buffer
  getContentKey(const time::system_clock::TimePoint& timeslot) const;

  /**
   * @brief Add @p key as the content key for the hour covering @p timeslot
   * @throws Error if a key for the same hour already exists
   */
  void
  addContentKey(const time::system_clock::TimePoint& timeslot, const Buffer& key);

  /**
   * @brief Delete content key for the hour covering @p timeslot
   */
  void
  deleteContentKey(const time::system_clock::TimePoint& timeslot);

  ////////////////////////////////////////////////////// schedule management

  /**
   * @brief Check if there is a schedule with @p name
   */
  bool
  hasSchedule(const std::string& name) const;

  /**
   * @brief List all the names of the schedules
   * @return A list of the name of all schedules.
   */
  std::list<std::string>
  listAllScheduleNames() const;

  /**
   * @brief Get a schedule with @p name.
   * @throw Error if the schedule does not exist
   */
  Schedule
  getSchedule(const std::string& name) const;

  /**
   * @brief Get member key name and public key buffer of a schedule with @p name.
   */
  std::map<Name, Buffer>
  getScheduleMembers(const std::string& name) const;

  std::map<Name, Buffer>
  getScheduleMembersL(const std::string& name) const;

  /**
   * @brief Add a @p schedule with @p name
   * @pre Name.length() != 0
   *
   * @throw Error if add operation fails, e.g., a schedule with the same name already exists
   */
  void
  addSchedule(const std::string& name, const Schedule& schedule);

  /**
   * @brief Delete the schedule with @p name.
   *        also delete members which reference the schedule.
   */
  void
  deleteSchedule(const std::string& name);

  /**
   * @brief Rename a schedule with @p oldName to @p newName
   * @pre newName.length() != 0
   *
   * @throw Error if update operation fails, e.g., a schedule with @p newName already exists
   */
  void
  renameSchedule(const std::string& oldName, const std::string& newName);

  /**
   * @brief Update the schedule with @p name and replace the old object with @p schedule
   *
   * if no schedule with @p name exists, a new schedule
   * with @p name and @p schedule will be added to database
   */
  void
  updateSchedule(const std::string& name, const Schedule& schedule);


  ////////////////////////////////////////////////////// content key management for location list

  /**
   * @brief Check if content key exists for gps coordinates @p latitude and @p longitude
   */
  bool
  hasContentKey(const double& latitude, const double& lonitude) const;

  /**
   * @brief Get content key for the gps coordinate @p latitude and @p longitude
   * @throws Error if the key does not exist
   */
  Buffer
  getContentKey(const double& latitude, const double& lonitude) const;

  /**
   * @brief Add @p key as the content key for the gps coordinate @p latitude and @p longitude
   * @throws Error if a key for the same hour already exists
   */
  void
  addContentKey(const double& latitude, const double& lonitude, const Buffer& key);

  /**
   * @brief Delete content key for the gps coordinate @p latitude and @p longitude
   */
  void
  deleteContentKey(const double& latitude, const double& lonitude);

  ////////////////////////////////////////////////////// content key management for time-location access policy

  /**
   * @brief Check if content key exists for gps coordinates @p latitude and @p longitude
   */
  bool
  hasContentKey(const Name& keyName) const;

  /**
   * @brief Get content key for the gps coordinate @p latitude and @p longitude
   * @throws Error if the key does not exist
   */
  Buffer
  getContentKey(const Name& keyName) const;

  /**
   * @brief Add @p key as the content key for the gps coordinate @p latitude and @p longitude
   * @throws Error if a key for the same hour already exists
   */
  void
  addContentKey(const Name& keyName, const Buffer& key);

  /**
   * @brief Delete content key for the gps coordinate @p latitude and @p longitude
   */
  void
  deleteContentKey(const Name& keyName);

  ////////////////////////////////////////////////////// Location list management

  /**
   * @brief Check if there is a location list with @p name
   */
  bool
  hasLocationList(const std::string& name) const;

  /**
   * @brief List all the names of the location lists.
   * @return A list of the name of the location lists.
   */
  std::list<std::string>
  listAllLocationListNames(const std::string& type) const;

  /**
   * @brief Get a location list with @p name.
   * @throw Error if the locationlist does not exist
   */
  LocationList
  getLocationList(const std::string& name) const;

  /**
   * @brief Get member key name and public key buffer of a location list with @p name.
   */
  std::map<Name, Buffer>
  getLocationListMembers(const std::string& name) const;

  /**
   * @brief Add a @p locationlist with @p name
   * @pre Name.length() != 0
   *
   * @throw Error if add operation fails, e.g., a locationlist with the same name already exists
   */
  void
  addLocationList(const std::string& name, const LocationList& locationlist);

  /**
   * @brief Delete the locationlist with @p name.
   *        also delete members which reference the locationlist.
   */
  void
  deleteLocationList(const std::string& name);

  /**
   * @brief Rename a locationlist with @p oldName to @p newName
   * @pre newName.length() != 0
   *
   * @throw Error if update operation fails, e.g., a locationlist with @p newName already exists
   */
  void
  renameLocationList(const std::string& oldName, const std::string& newName);

  /**
   * @brief Update the locationlist with @p name and replace the old object with @p schedule
   *
   * if no schedule with @p name exists, a new locationlist
   * with @p name and @p locationlist will be added to database
   */
  void
  updateLocationList(const std::string& name, const LocationList& locationlist);

  ////////////////////////////////////////////////////// Time-Location list management

  /**
   * @brief Check if there is a location list with @p name
   */
  bool
  hasTimeLocationList(const std::string& name) const;

  /**
   * @brief List all the names of the location lists.
   * @return A list of the name of the location lists.
   */
  std::list<std::string>
  listAllTimeLocationListNames() const;

  /**
   * @brief Get a location list with @p name.
   * @throw Error if the locationlist does not exist
   */
  TimeLocationList
  getTimeLocationList(const std::string& name) const;

  /**
   * @brief Get member key name and public key buffer of a location list with @p name.
   */
  std::map<Name, Buffer>
  getTimeLocationListMembers(const std::string& name) const;

  /**
   * @brief Add a @p locationlist with @p name
   * @pre Name.length() != 0
   *
   * @throw Error if add operation fails, e.g., a locationlist with the same name already exists
   */
  void
  addTimeLocationList(const std::string& name, const TimeLocationList& locationlist);

  /**
   * @brief Delete the locationlist with @p name.
   *        also delete members which reference the locationlist.
   */
  void
  deleteTimeLocationList(const std::string& name);

  /**
   * @brief Rename a locationlist with @p oldName to @p newName
   * @pre newName.length() != 0
   *
   * @throw Error if update operation fails, e.g., a locationlist with @p newName already exists
   */
  void
  renameTimeLocationList(const std::string& oldName, const std::string& newName);

  /**
   * @brief Update the locationlist with @p name and replace the old object with @p schedule
   *
   * if no schedule with @p name exists, a new locationlist
   * with @p name and @p locationlist will be added to database
   */
  void
  updateTimeLocationList(const std::string& name, const TimeLocationList& locationlist);


  ////////////////////////////////////////////////////// member management

  /**
   * @brief Check if there is a member with name @p identity
   */
  bool
  hasMember(const Name& identity) const;

  /**
   * @brief List all the members
   */
  std::list<Name>
  listAllMembers() const;

  /**
   * @brief Get the schedule name of a member with name @p identity
   *
   * @throw Error if there is no member with name @p identity in database
   */
  std::string
  getMemberSchedule(const Name& identity) const;

  /**
   * @brief Add a new member with @p key of @p keyName
   *        into a schedule with name @p scheduleName.
   *
   * @throw Error when there's no schedule named @p scheduleName
   * @throw Error if add operation fails, e.g., the added member exists
   */
  void
  addMember(const std::string& scheduleName, const Name& keyName, const Buffer& key);


  /**
   * @brief Check if there is a member with name @p identity
   */
  bool
  hasMemberLocation(const Name& identity) const;

  void
  addMemberLocation(const std::string& listName, const Name& keyName, const Buffer& key);

  void
  addMemberTimeLocation(const std::string& listName, const Name& keyName, const Buffer& key);

  /**
   * @brief Change the schedule of a member with name @p identity to a schedule with @p scheduleName
   *
   * @throw Error when there's no schedule named @p scheduleName
   */
  void
  updateMemberSchedule(const Name& identity, const std::string& scheduleName);

  /**
   * @brief Delete a member with name @p identity from database
   */
  void
  deleteMember(const Name& identity);

  /**
   * @brief Check if there is a EKey with name @p eKeyName in database
   */
  bool
  hasEKey(const Name& eKeyName);
  
  bool
  hasKekCert(const Name& eKeyName);

  /**
   * @brief Add a EKey with name @p eKeyName to database
   *
   * @p pubKey The public Key of the group key pair
   * @p priKey The private Key of the group key pair
   */
  void
  addEKey(const Name& eKeyName, const Buffer& pubKey);

  void
  addKekCert(const Name& eKeyName, const Certificate& newCert);

  /**
   * @brief Get the group key pair from database
   */
  Buffer
  getEKey(const Name& eKeyName);

  Certificate
  getKekCert(const Name& eKeyName);

  /**
   * @brief Delete all the EKeys in the database
   *
   * The database will keep growing because EKeys will keep being added. The method
   * should be called periodically
   */
  void
  cleanEKeys();

  /**
   * @brief Delete a EKey with name @p eKeyName from database
   */
  void
  deleteEKey(const Name& eKeyName);

private:
  class Impl;
  unique_ptr<Impl> m_impl;
};

} // namespace nac
} // namespace ndn

#endif // NDN_NAC_PRODUCER_DB_HPP
