/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/**
 * Copyright (c) 2014-2018, Regents of the University of California
 *
 * This file is part of NAC (Name-Based Access Control for NDN).
 * See AUTHORS.md for complete list of NAC authors and contributors.
 *
 * NAC is free software: you can redistribute it and/or modify it under the terms
 * of the GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * NAC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * NAC, e.g., in COPYING.md file.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Zhiyi Zhang <zhiyi@cs.ucla.edu>
 */

#ifndef NDN_NAC_TIME_LOCATION_HPP
#define NDN_NAC_TIME_LOCATION_HPP

#include "common.hpp"
#include <boost/date_time/posix_time/posix_time.hpp>

namespace ndn {
namespace nac {

typedef boost::posix_time::ptime TimeStamp;

///@brief Interval define a time duration which contains a start timestamp and an end timestamp
class TimeLocation
{
public:
  class Error : public std::runtime_error
  {
  public:
    using std::runtime_error::runtime_error;
  };

public:
  enum class
  RepeatUnit {
    NONE = 0,
    DAY = 1,
    MONTH = 2,
    YEAR = 3
  };

public:
  TimeLocation(bool isValid = false);

  TimeLocation(const Block& block);

  TimeLocation(const double& latitude, 
               const double& longitude, 
               const double& radius,
               const TimeStamp& startTime, 
               const TimeStamp& endTime);

  TimeLocation(const double& latitude, 
               const double& longitude, 
               const double& radius,
               const TimeStamp& startDate,
               const TimeStamp& endDate,
               size_t intervalStartHour,
               size_t intervalEndHour,
               size_t nRepeats = 0,
               RepeatUnit unit = RepeatUnit::NONE);

  template<encoding::Tag TAG>
  size_t
  wireEncode(EncodingImpl<TAG>& encoder) const;

  const Block&
  wireEncode() const;

  void
  wireDecode(const Block& wire);

//   /**
//    * @brief Check if the timestamp tp is in the interval
//    * @pre this->isValid() == true
//    *
//    * @param tp A timestamp
//    */
//   bool
//   covers(const double& latitude, const double& longitude) const;

//   bool
//   covers(const TimeStamp& tp) const;


  std::tuple<bool, TimeLocation>
  getTimeLocation(const TimeStamp& tp, const double& latitude, const double& longitude, const double& radius) const;

  /**
   * @brief Get the intersection interval of two intervals
   * @pre this->isValid() == true && interval.isValid() == true
   *
   * Two intervals should all be valid but they can be empty
   */
  TimeLocation&
  operator&&(const TimeLocation& interval);

  /**
   * @brief Get the union set interval of two intervals
   * @pre this->isValid() == true && interval.isValid() == true
   *
   * Two intervals should all be valid but they can be empty
   */
  TimeLocation&
  operator||(const TimeLocation& interval);

  /**
   * @brief To store in std::set, class have to implement operator <
   *
   * @param interval Interval which will be compared with
   */
  bool
  operator<(const TimeLocation& location) const;

  const double&
  getLatitude() const
  {
    return m_latitude;
  }

  const double&
  getLongitude() const
  {
    return m_longitude;
  }

  const double&
  getRadius() const
  {
    return m_radius;
  }

  bool
  isValid() const
  {
    return m_isValid;
  }

  bool
  isEmpty() const
  {
    BOOST_ASSERT(isValid());
    return m_startTime == m_endTime;
  }

  const TimeStamp&
  getStartDate() const
  {
    return m_startDate;
  }

  const TimeStamp&
  getEndDate() const
  {
    return m_endDate;
  }

  size_t
  getIntervalStartHour() const
  {
    return m_intervalStartHour;
  }

  size_t
  getIntervalEndHour() const
  {
    return m_intervalEndHour;
  }

  size_t
  getNRepeats() const
  {
    return m_nRepeats;
  }

  RepeatUnit
  getRepeatUnit() const
  {
    return m_unit;
  }

  const TimeStamp&
  getStartTime() const
  {
    BOOST_ASSERT(isValid());
    return m_startTime;
  }

  const TimeStamp&
  getEndTime() const
  {
    BOOST_ASSERT(isValid());
    return m_endTime;
  }

private:
  ///@brief Check if there is any interval in the date of timestamp
  bool
  hasIntervalOnDate(const TimeStamp& tp) const;

  double m_latitude;
  double m_longitude;
  double m_radius;

  TimeStamp m_startTime;
  TimeStamp m_endTime;

  TimeStamp m_startDate;
  TimeStamp m_endDate;
  size_t m_intervalStartHour;
  size_t m_intervalEndHour;
  size_t m_nRepeats;
  RepeatUnit m_unit;

  bool m_isValid;
  mutable Block m_wire;
};

} // namespace nac
} // namespace ndn

#endif // NDN_NAC_TIME_LOCATION_HPP
