/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/**
 * Copyright (c) 2014-2018, Regents of the University of California
 *
 * This file is part of NAC (Name-Based Access Control for NDN).
 * See AUTHORS.md for complete list of NAC authors and contributors.
 *
 * NAC is free software: you can redistribute it and/or modify it under the terms
 * of the GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * NAC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * NAC, e.g., in COPYING.md file.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Zhiyi Zhang <zhiyi@cs.ucla.edu>
 */

#include "location.hpp"
#include <ndn-cxx/encoding/block-helpers.hpp>

#include <math.h>  
#include <algorithm>
#include <iostream>

namespace ndn {
namespace nac {

Location::Location(bool isValid)
  : m_latitude(43.6121)
  , m_longitude(-116.3915)
  , m_radius(0)
  , m_isValid(isValid)
{
}

Location::Location(const Block& block)
{
  wireDecode(block);
}

Location::Location(const double& latitude, const double& longitude, const double& radius)
  : m_latitude(latitude)
  , m_longitude(longitude)
  , m_radius(radius)
  , m_isValid(true)
{
  
}

template<encoding::Tag TAG>
size_t
Location::wireEncode(EncodingImpl<TAG>& encoder) const
{
  size_t totalLength = 0;

  totalLength += prependDoubleBlock(encoder, tlv::Radius, m_radius);
  totalLength += prependDoubleBlock(encoder, tlv::CenterLongitude, m_longitude);
  totalLength += prependDoubleBlock(encoder, tlv::CenterLatitude, m_latitude);

  totalLength += encoder.prependVarNumber(totalLength);
  totalLength += encoder.prependVarNumber(tlv::Location);

  return totalLength;
}

const Block&
Location::wireEncode() const
{
  if (m_wire.hasWire())
    return m_wire;

  EncodingEstimator estimator;
  size_t estimatedSize = wireEncode(estimator);

  EncodingBuffer buffer(estimatedSize, 0);
  wireEncode(buffer);

  this->m_wire = buffer.block();
  return m_wire;
}

void
Location::wireDecode(const Block& wire)
{
  using namespace boost::posix_time;

  if (wire.type() != tlv::Location)
    BOOST_THROW_EXCEPTION(tlv::Error("Unexpected TLV type when decoding Location"));

  m_wire = wire;
  m_wire.parse();

  if (m_wire.elements_size() != 3)
    BOOST_THROW_EXCEPTION(tlv::Error("Location tlv does not have six sub-TLVs"));

  Block::element_const_iterator it = m_wire.elements_begin();

  if (it->type() == tlv::CenterLatitude) {
    m_latitude = encoding::readDouble(*it);
    it++;
  }
  else
    BOOST_THROW_EXCEPTION(tlv::Error("First element must be CenterLatitude"));


  if (it->type() == tlv::CenterLongitude) {
    m_longitude = encoding::readDouble(*it);
    it++;
  }
  else
    BOOST_THROW_EXCEPTION(tlv::Error("Second element must be CenterLongitude"));

  if (it->type() == tlv::Radius) {
    m_radius = encoding::readDouble(*it);
    it++;
  }
  else
    BOOST_THROW_EXCEPTION(tlv::Error("Third element must be Radius"));

}

std::tuple<bool, Location>
Location::getLocation(const double& latitude, const double& longitude, const double& radius) const
{
  double lat;
  double lon;
  double rad;
  bool isPositive;

  if (m_latitude == latitude && m_longitude == longitude && m_radius == radius) 
  {
    isPositive = true;
    lat = m_latitude;
    lon = m_longitude;
    rad = m_radius;
    }
  else {
    lat = m_latitude;
    lon = m_longitude;
    rad = 0;
    isPositive = false;
  }

  return std::make_tuple(isPositive, Location(lat, lon, rad));
}

bool
Location::operator<(const Location& location) const
{
  if (m_latitude < location.getLatitude())
    return true;
  else if (m_latitude > location.getLatitude())
    return false;

  if (m_longitude < location.getLongitude())
    return true;
  else if (m_longitude > location.getLongitude())
    return false;

  if (m_radius < location.getRadius())
    return true;
  else if (m_radius > location.getRadius())
    return false;
}


} // namespace nac
} // namespace ndn
