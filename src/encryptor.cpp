/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2014-2019, Regents of the University of California
 *
 * NAC library is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * NAC library is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.
 *
 * You should have received copies of the GNU General Public License and GNU Lesser
 * General Public License along with ndn-cxx, e.g., in COPYING.md file.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * See AUTHORS.md for complete list of NAC library authors and contributors.
 */

#include "encryptor.hpp"

#include <ndn-cxx/util/logger.hpp>
#include <boost/lexical_cast.hpp>

#include <iostream>

#define PI 3.14159265

namespace ndn {
namespace nac {

// static int count = 0;

NDN_LOG_INIT(nac.Encryptor);

const size_t N_RETRIES = 3;

static double 
toRadians(double degree)
{
    double rad;
    rad =  ( degree * PI ) / 180 ;
    return rad;
}

std::vector<double>
boundingCoordinates(double lat, double lon, double radius) {
    
  std::vector<double> result;
  double radDist = radius / 6371000.0;

  double minLatitude = toRadians(lat) - radDist;
  double maxLatitude = toRadians(lat) + radDist;
  double minLongitude;
  double maxLongitude;

  if (minLatitude > toRadians(-90.0) && maxLatitude < toRadians(90.0)) {
    double deltaLon = asin(sin(radDist) /cos(toRadians(lat)));
    minLongitude = toRadians(lon) - deltaLon;
    if (minLongitude < toRadians(-180.0)) minLongitude += 2 * PI;
    maxLongitude = toRadians(lon) + deltaLon;
    if (maxLongitude > toRadians(180.0)) maxLongitude -= 2 * PI;
  } else {
    // a pole is within the distance
    minLatitude = std::max(minLatitude, toRadians(-90.0));
    maxLatitude = std::min(maxLatitude, toRadians(90.0));
    minLongitude = toRadians(-180);
    maxLongitude = toRadians(180);
  }
	
	result.push_back(minLatitude);
	result.push_back(maxLatitude);
	result.push_back(minLongitude);
	result.push_back(maxLongitude);
  result.push_back(radDist);
	
	return result;
}

static const time::system_clock::TimePoint
getCkEndTimeslot(const time::system_clock::TimePoint& timeslot, const time::milliseconds granuality)
{
  // return time::fromUnixTimestamp((time::toUnixTimestamp(timeslot) / 3600000) * 3600000);
  return time::fromUnixTimestamp(time::toUnixTimestamp(timeslot) + granuality);
}

Encryptor::Encryptor(const Name& accessPrefix,
                     const Name& ckPrefix, SigningInfo ckDataSigningInfo,
                     const ErrorCallback& onFailure,
                     Validator& validator, KeyChain& keyChain, Face& face)
  : m_accessPrefix{accessPrefix}
  , m_ckPrefix{ckPrefix}
  , m_ckBits{AES_KEY_SIZE}
  , m_ckDataSigningInfo{std::move(ckDataSigningInfo)}
  , m_db(":memory:")
  , m_isKekRetrievalInProgress(false)
  , m_onFailure(onFailure)
  , m_keyChain{keyChain}
  , m_face{face}
  , m_scheduler{face.getIoService()}
{
  regenerateCk();

  auto serveFromIms = [this] (const Name&, const Interest& interest) {
    auto data = m_ims.find(interest);
    if (data != nullptr) {
      NDN_LOG_DEBUG("Serving " << data->getName() << " from InMemoryStorage");
      m_face.put(*data);
    }
    else {
      NDN_LOG_DEBUG("Didn't find CK data for " << interest.getName());
      // send NACK?
    }
  };

  auto handleError = [] (const Name& prefix, const std::string& msg) {
    NDN_LOG_ERROR("Failed to register prefix " << prefix << ": " << msg);
  };

  m_ckReg = m_face.setInterestFilter(Name(ckPrefix).append(CK), serveFromIms, handleError);
}

Encryptor::Encryptor(const Name& accessPrefix,
                     const Name& ckPrefix, SigningInfo ckDataSigningInfo,
                     const std::string& db_path,
                     const std::string starttime,
                     size_t granularity,
                     RepeatUnit unit,
                     const std::vector< std::vector<double>>  locations,
                     const ErrorCallback& onFailure,
                     Validator& validator, KeyChain& keyChain, Face& face)
  : m_accessPrefix{accessPrefix}
  , m_ckPrefix{ckPrefix}
  , m_ckBits{AES_KEY_SIZE}
  , m_ckDataSigningInfo{std::move(ckDataSigningInfo)}
  , m_db(db_path)
  , m_granularity(granularity)
  , m_unit(unit)
  , m_locations(locations)
  , m_isKekRetrievalInProgress(false)
  , m_onFailure(onFailure)
  , m_keyChain{keyChain}
  , m_face{face}
  , m_scheduler{face.getIoService()}
{
  KeyInfo keyInfo;
  LocationKeyInfo keyInfoLocation;
  TimeLocationKeyInfo keyInfoTimeLocation;
  Name nodeName = accessPrefix;
  m_ekeyInfo[nodeName] = keyInfo;
  m_ekeyInfoLocation[nodeName] = keyInfoLocation;
  m_ekeyInfoTimeLocation[nodeName] = keyInfoTimeLocation;

  m_first = true;
  m_count = 0;

  m_isKekExist = false;

  if (m_unit == Encryptor::RepeatUnit::SECOND) {
    m_accessGranularity = time::milliseconds(m_granularity*1000);
    // std::cout << "m_granularity"<<m_accessGranularity<< std::endl;
  }
  else if (m_unit == Encryptor::RepeatUnit::MINUTE) {
    m_accessGranularity = time::milliseconds(m_granularity*60000);
    // std::cout << "m_granularity"<<m_accessGranularity<< std::endl;

  }
  else if (m_unit == Encryptor::RepeatUnit::HOUR) {
    m_accessGranularity = time::milliseconds(m_granularity*3600000);
    // std::cout << "m_granularity"<<m_accessGranularity<< std::endl;
  }
  else if (m_unit == Encryptor::RepeatUnit::DAY) {
    m_accessGranularity = time::milliseconds(m_granularity*86400000);
  }

}

Encryptor::~Encryptor()
{
  m_kekPendingInterest.cancel();
}

void
Encryptor::retryFetchingKek()
{
  if (m_isKekRetrievalInProgress) {
    return;
  }

  std::cout << "Retrying fetching of KEK" << std::endl;

  NDN_LOG_DEBUG("Retrying fetching of KEK");
  m_isKekRetrievalInProgress = true;
  fetchKekAndPublishCkData([&] {
      NDN_LOG_DEBUG("KEK retrieved and published");

      std::cout << "KEK retrieved and Encrypted-CK published" << std::endl;

      m_isKekRetrievalInProgress = false;
    },
    [=] (const ErrorCode& code, const std::string& msg) {

      std::cout << "Failed to retrieved KEK" << std::endl;

      NDN_LOG_ERROR("Failed to retrieved KEK: " + msg);
      m_isKekRetrievalInProgress = false;
      m_onFailure(code, msg);
    },
    N_RETRIES);
}

void
Encryptor::regenerateCk()
{
  m_ckName = m_ckPrefix;
  m_ckName
    .appendVersion(); // version = ID of CK
  NDN_LOG_DEBUG("Generating new CK: " << m_ckName);
  random::generateSecureBytes(m_ckBits.data(), m_ckBits.size());

  // one implication: if CK updated before KEK fetched, KDK for the old CK will not be published
  if (!m_kek) {
    retryFetchingKek();
  }
  else {
    makeAndPublishCkData(m_onFailure);
  }
}

void
Encryptor::onInterestCK(const InterestFilter& filter, const Interest& interest)
{

  auto data = m_ims.find(interest.getName());

  if (data != nullptr) {
    NDN_LOG_DEBUG("Serving " << data->getName() << " from InMemoryStorage");
    std::cout << "Serving encrypted CK from InMemoryStorage " << *data << std::endl;
    m_face.put(*data);
  }
  else {
    NDN_LOG_DEBUG("Didn't find CK data for " << interest.getName());
    std::cout << "Cannot find content key in storage.  " << interest.getName() << std::endl;

    // send NACK?
  }

}

void
Encryptor::onRegisterFailedCK(const Name& prefix, const std::string& reason)
{
  std::cerr << "ERROR: Failed to register prefix \""
            << prefix << "\" in local hub's daemon (" << reason << ")"
            << std::endl;
  m_face.shutdown();
}


void
Encryptor::getEncryptedCK()
{
  m_face.setInterestFilter("/org/md2k/DATA/CK",
                            bind(&Encryptor::onInterestCK, this, _1, _2),
                            RegisterPrefixSuccessCallback(),
                            bind(&Encryptor::onRegisterFailedCK, this, _1, _2));

}

EncryptedContent
Encryptor::encrypt(const uint8_t* data, size_t size)
{
  // Generate initial vector
  auto iv = make_shared<Buffer>(AES_IV_SIZE);
  random::generateSecureBytes(iv->data(), iv->size());

  OBufferStream os;
  security::transform::bufferSource(data, size)
    >> security::transform::blockCipher(BlockCipherAlgorithm::AES_CBC,
                                        CipherOperator::ENCRYPT,
                                        m_ckBits.data(), m_ckBits.size(), iv->data(), iv->size())
    >> security::transform::streamSink(os);

  EncryptedContent content;
  content.setIv(iv);
  content.setPayload(os.buf());
  content.setKeyLocator(m_ckName);

  return content;
}

void
Encryptor::encrypt(const time::system_clock::TimePoint& timeslot, 
                   const double& latitude, const double& longitude,
                   const uint8_t* data, size_t size,
                   const dataCallback& onData)
{
  size_t r = m_locations.size(); // get the count of location

  if (m_first)
  {
    m_starttime = time::toIsoString(timeslot);
    m_first = false;
  }

  for(size_t i = 0; i < r; i++) //iterate location list
  {
    std::vector<double> re = boundingCoordinates(m_locations[i][0], m_locations[i][1], m_locations[i][2]);

    double r = acos(sin(toRadians(m_locations[i][0]))*sin(toRadians(latitude)) + cos(toRadians(m_locations[i][0]))*cos(toRadians(latitude))*cos(toRadians(longitude) - (toRadians(m_locations[i][1]))));

    if (toRadians(latitude) >= re[0] && toRadians(latitude) <= re[1] && toRadians(longitude) >= re[2] && toRadians(longitude) <= re[3] && r <= re[4]) 
    {
      const time::system_clock::TimePoint endslot = getCkEndTimeslot(time::fromIsoString(m_starttime), m_accessGranularity);

      if ( timeslot >= time::fromIsoString(m_starttime) && timeslot < endslot) // check if current CK time interval covers the timestamp of the datapoint.
      {
        //Create content key name.
        m_ckName = m_ckPrefix;
        m_ckName
          .append(m_starttime)
          .append(time::toIsoString(endslot))
          .append(std::to_string(m_locations[i][0]))
          .append(std::to_string(m_locations[i][1]))
          .append(std::to_string(m_locations[i][2])); 

      }
      else // create new time interval for ck
      {
        m_starttime = time::toIsoString(timeslot);
        const time::system_clock::TimePoint endslot = getCkEndTimeslot(time::fromIsoString(m_starttime), m_accessGranularity);

        //Create content key name.
        m_ckName = m_ckPrefix;
        m_ckName
          .append(m_starttime)
          .append(time::toIsoString(endslot))
          .append(std::to_string(m_locations[i][0]))
          .append(std::to_string(m_locations[i][1]))
          .append(std::to_string(m_locations[i][2])); 
      }

      if (m_db.hasContentKey(m_ckName)) {
        // We have created the content key, get the content key directly.
        std::cout << "We have created the content key, get the content key directly. " << m_ckName << std::endl;
        m_ckBits = m_db.getContentKey(m_ckName);
      }
      else {
        m_count +=1;
        std::cout << "generate ck: " << m_ckName << std::endl;
        random::generateSecureBytes(m_ckBits.data(), m_ckBits.size());
        m_db.addContentKey(m_ckName, m_ckBits); 
      }
    }
  }
  
  // Now we need to retrieve the E-KEYs for content key encryption.
  // Check if current E-KEYs can cover the content key.

  time::system_clock::TimePoint s_timeslot;
  s_timeslot = time::fromIsoString(m_ckName.get(-5).toUri());

  time::system_clock::TimePoint e_timeslot;
  e_timeslot = time::fromIsoString(m_ckName.get(-4).toUri());

  m_ts = s_timeslot;
  m_lat = std::stod(m_ckName.get(-3).toUri());
  m_lon = std::stod(m_ckName.get(-2).toUri());
  m_rad = std::stod(m_ckName.get(-1).toUri());
  m_isKekExist = false;

  std::cout << "Check if current KEKs can cover the content key. " << std::endl;
  std::unordered_map<Name, TimeLocationKeyInfo>::iterator it;
  for (it = m_ekeyInfoTimeLocation.begin(); it != m_ekeyInfoTimeLocation.end(); ++it) {
    if ( m_lat == it->second.latitude && m_lon == it->second.longitude && m_rad == it->second.radius
      && m_ts >= it->second.beginTimeslot && m_ts <= it->second.endTimeslot)
    {
        // current E-KEY can cover the content key, encrypt the content key directly.
        Data kek = it->second.key;
        std::cout << "Current KEK can cover the content key. "  << std::endl;
        m_kekName = it->first;
        makeAndPublishCkData(kek, m_onFailure);
        m_isKekExist = true;
        break;
    }

  }

  std::unordered_map<Name, KeyInfo>::iterator it1;
  for (it1 = m_ekeyInfo.begin(); it1 != m_ekeyInfo.end(); ++it1) {
    if (m_ts >= it1->second.beginTimeslot && m_ts <= it1->second.endTimeslot)
    {
        // current E-KEY can cover the content key, encrypt the content key directly.
        Data kek = it1->second.key;
        std::cout << "Current time-KEK can cover the content key. "  << std::endl;
        m_kekName = it1->first;
        makeAndPublishCkData(kek, m_onFailure);
        m_isKekExist = true;
        break;
    }
  }

  std::unordered_map<Name, LocationKeyInfo>::iterator it2;
  for (it2 = m_ekeyInfoLocation.begin(); it2 != m_ekeyInfoLocation.end(); ++it2) {
    if ( m_lat == it2->second.latitude && m_lon == it2->second.longitude && m_rad == it2->second.radius)
    {
        // current E-KEY can cover the content key, encrypt the content key directly.
        Data kek = it2->second.key;
        std::cout << "Current Location-KEK can cover the content key. "  << std::endl;
        m_kekName = it2->first;
        makeAndPublishCkData(kek, m_onFailure);
        m_isKekExist = true;
        break;
    }

  }

  if(!m_isKekExist) {
    std::cout << "Current KEK cannot cover the content key, retrieve one. "  << std::endl;

    fetchKekAndPublishCkData([=] {
      NDN_LOG_DEBUG("KEK retrieved and published");
      std::cout << "KEK retrieved and CK published" << std::endl;

      // Generate initial vector
      auto iv = make_shared<Buffer>(AES_IV_SIZE);
      random::generateSecureBytes(iv->data(), iv->size());
      OBufferStream os;
      security::transform::bufferSource(data, size)
        >> security::transform::blockCipher(BlockCipherAlgorithm::AES_CBC,
                                            CipherOperator::ENCRYPT,
                                            m_ckBits.data(), m_ckBits.size(), iv->data(), iv->size())
        >> security::transform::streamSink(os);
      EncryptedContent content;
      content.setIv(iv);
      content.setPayload(os.buf());
      content.setKeyLocator(m_ckName);

      onData(content);
    },
    [=] (const ErrorCode& code, const std::string& msg) {

      std::cout << "Failed to retrieved KEK:" << std::endl;
      NDN_LOG_ERROR("Failed to retrieved KEK: " + msg);
      m_onFailure(code, msg);
    },
    N_RETRIES);

  }
  else {
    // Generate initial vector
    auto iv = make_shared<Buffer>(AES_IV_SIZE);
    random::generateSecureBytes(iv->data(), iv->size());
    OBufferStream os;
    security::transform::bufferSource(data, size)
      >> security::transform::blockCipher(BlockCipherAlgorithm::AES_CBC,
                                          CipherOperator::ENCRYPT,
                                          m_ckBits.data(), m_ckBits.size(), iv->data(), iv->size())
      >> security::transform::streamSink(os);
    EncryptedContent content;
    content.setIv(iv);
    content.setPayload(os.buf());
    content.setKeyLocator(m_ckName);

    onData(content);
  }
}

void
Encryptor::fetchKekAndPublishCkData(const std::function<void()>& onReady,
                                    const ErrorCallback& onFailure,
                                    size_t nTriesLeft)
{
  // interest for <access-prefix>/KEK to retrieve <access-prefix>/KEK/<key-id> KekData
  NDN_LOG_DEBUG("Fetching KEK " << Name(m_accessPrefix).append(KEK));
  std::cout << "Fetching KEK...." << std::endl;

  Interest kekInterest;
  if (m_ckName.get(-2) == CK) {
    kekInterest.setName(Name(m_accessPrefix).append(KEK));
  }
  else{
    kekInterest.setName(Name(m_accessPrefix).append(KEK).append(time::toIsoString(m_ts)).append(to_string(m_lat)).append(to_string(m_lon)).append(to_string(m_rad)));
  }

  m_kekPendingInterest = m_face.expressInterest(kekInterest.setCanBePrefix(true)
                                                           .setMustBeFresh(true),
    [=] (const Interest& i, const Data& kek) {

      // @todo verify if the key is legit
      Name name = kek.getName();
      Name subName;
      subName = kek.getName().getSubName(m_accessPrefix.size() + 5);

      Name kekPrefix = m_accessPrefix;

      Name keyName = kekPrefix.append(subName);

      // std::cout << "kekName " << keyName << std::endl;

      //check if kek contains time interval
      if (kek.getName().get(-2)== KEK) {
        m_kek = kek;
        if (makeAndPublishCkData(onFailure)) {
          onReady();
        }
      }
      else if (subName.get(3)== name::Component("*")) {
        time::system_clock::TimePoint begin = time::fromIsoString(keyName.get(-6).toUri());
        time::system_clock::TimePoint end = time::fromIsoString(keyName.get(-5).toUri());

        Name interestName = keyName;
         std::cout << "\n time get KEK data and store it: " <<i.getName() << "\n"<<keyName << std::endl;
        m_kekName = interestName;

        m_kek = kek;

        if (makeAndPublishCkData(kek, onFailure)) {

          m_ekeyInfo[interestName].beginTimeslot = begin;
          m_ekeyInfo[interestName].endTimeslot = end;
          m_ekeyInfoTimeLocation[interestName].key = kek;
          m_ekeyInfo[interestName].key = kek;

          onReady();
        }
      }
      else if (subName.get(1)== name::Component("*")){
        double centerLat = std::stod(keyName.get(-4).toUri());
        double centerLon = std::stod(keyName.get(-3).toUri());
        double radius = std::stod(keyName.get(-2).toUri());

        Name interestName = keyName;
        std::cout << "location get KEK data and store it: " << interestName << std::endl;
        m_kekName = interestName;

          m_kek = kek;

        if (makeAndPublishCkData(kek, onFailure)) {
          m_ekeyInfoLocation[interestName].latitude = centerLat;
          m_ekeyInfoLocation[interestName].longitude = centerLon;
          m_ekeyInfoLocation[interestName].radius = radius;
          m_ekeyInfoTimeLocation[interestName].key = kek;

          onReady();
        }
      }
      else {
        time::system_clock::TimePoint begin = time::fromIsoString(keyName.get(-6).toUri());
        time::system_clock::TimePoint end = time::fromIsoString(keyName.get(-5).toUri());
        double centerLat = std::stod(keyName.get(-4).toUri());
        double centerLon = std::stod(keyName.get(-3).toUri());
        double radius = std::stod(keyName.get(-2).toUri());

        Name interestName = keyName;
        std::cout << "get KEK data and store it: " << interestName << std::endl;
        m_kekName = interestName;

          m_kek = kek;

        if (makeAndPublishCkData(kek, onFailure)) {
          m_ekeyInfoTimeLocation[interestName].beginTimeslot = begin;
          m_ekeyInfoTimeLocation[interestName].endTimeslot = end;
          m_ekeyInfoTimeLocation[interestName].latitude = centerLat;
          m_ekeyInfoTimeLocation[interestName].longitude = centerLon;
          m_ekeyInfoTimeLocation[interestName].radius = radius;
          m_ekeyInfoTimeLocation[interestName].key = kek;
          onReady();
        }
      }
      // otherwise, failure has been already declared
    },
    [=] (const Interest& i, const lp::Nack& nack) {
      std::cout << "get NACK " << boost::lexical_cast<std::string>(nack.getReason()) <<std::endl;
      if (nTriesLeft > 1) {
        std::cout << "RETRY_DELAY_AFTER_NACK " << std::endl;
        m_scheduler.schedule(RETRY_DELAY_AFTER_NACK, [=] {
          fetchKekAndPublishCkData(onReady, onFailure, nTriesLeft - 1);
        });
      }
      else {

         std::cout << "RGot NACK with reason " << boost::lexical_cast<std::string>(nack.getReason())<<std::endl;

        onFailure(ErrorCode::KekRetrievalFailure, "Retrieval of KEK [" + i.getName().toUri() +
                  "] failed. Got NACK with reason " + boost::lexical_cast<std::string>(nack.getReason()));
        NDN_LOG_DEBUG("Scheduling retry from NACK");
        m_scheduler.schedule(RETRY_DELAY_KEK_RETRIEVAL, [this] { retryFetchingKek(); });
      }
    },
    [=] (const Interest& i) {

      std::cout << "get Timeput " <<i.getName() <<std::endl;
         std::cout << "get Timeput " <<i.getName() <<std::endl;
      if (nTriesLeft > 1) {
        fetchKekAndPublishCkData(onReady, onFailure, nTriesLeft - 1);
      }
      else {
        onFailure(ErrorCode::KekRetrievalTimeout,
                  "Retrieval of KEK [" + i.getName().toUri() + "] timed out");
        NDN_LOG_DEBUG("Scheduling retry after all timeouts");
        m_scheduler.schedule(RETRY_DELAY_KEK_RETRIEVAL, [this] { retryFetchingKek(); });
      }
    });
}

bool
Encryptor::makeAndPublishCkData(const Data& eKey, const ErrorCallback& onFailure)
{
  try {

    PublicKey kek;
    kek.loadPkcs8(eKey.getContent().value(), eKey.getContent().value_size());

    m_ckBits = m_db.getContentKey(m_ckName);
    EncryptedContent content;
     
    content.setPayload(kek.encrypt(m_ckBits.data(), m_ckBits.size()));

    auto ckData = make_shared<Data>(Name(m_ckName).append(ENCRYPTED_BY).append(m_kekName));

    ckData->setContent(content.wireEncode());

    // FreshnessPeriod can serve as a soft access control for revoking access
    ckData->setFreshnessPeriod(DEFAULT_CK_FRESHNESS_PERIOD);

    m_keyChain.sign(*ckData, m_ckDataSigningInfo);
    m_ims.insert(*ckData);

    NDN_LOG_DEBUG("Publishing CK data: " << ckData->getName());
    std::cout << "Publishing CK data: "  << ckData->getName() << "\n" << std::endl;
    return true;
  }
  catch (const std::runtime_error& e) {

    std::cout << e.what() << "\n";

     std::cout << "Failed to encrypt CK with KEK " << std::endl;
    onFailure(ErrorCode::EncryptionFailure, "Failed to encrypt generated CK with KEK " + eKey.getName().toUri());
    return false;
  }
}

bool
Encryptor::makeAndPublishCkData(const ErrorCallback& onFailure)
{
  try {
    PublicKey kek;
    kek.loadPkcs8(m_kek->getContent().value(), m_kek->getContent().value_size());

    EncryptedContent content;
    content.setPayload(kek.encrypt(m_ckBits.data(), m_ckBits.size()));

    auto ckData = make_shared<Data>(Name(m_ckName).append(ENCRYPTED_BY).append(m_kek->getName()));
    ckData->setContent(content.wireEncode());
    // FreshnessPeriod can serve as a soft access control for revoking access
    ckData->setFreshnessPeriod(DEFAULT_CK_FRESHNESS_PERIOD);
    m_keyChain.sign(*ckData, m_ckDataSigningInfo);
    m_ims.insert(*ckData);

    NDN_LOG_DEBUG("Publishing CK data: " << ckData->getName());
    return true;
  }
  catch (const std::runtime_error&) {

     std::cout << "Failed to encrypt generated CK with KEK " << std::endl;
    onFailure(ErrorCode::EncryptionFailure, "Failed to encrypt generated CK with KEK " + m_kek->getName().toUri());
    return false;
  }
}

} // namespace nac
} // namespace ndn
