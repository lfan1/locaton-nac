/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2014-2019, Regents of the University of California
 *
 * NAC library is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * NAC library is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.
 *
 * You should have received copies of the GNU General Public License and GNU Lesser
 * General Public License along with ndn-cxx, e.g., in COPYING.md file.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * See AUTHORS.md for complete list of NAC library authors and contributors.
 */

#include "access-manager.hpp"
#include "encrypted-content.hpp"

#include <ndn-cxx/util/logger.hpp>

#include <map>
#include <iostream>

NDN_LOG_INIT(nac.AccessManager);

namespace ndn {
namespace nac {

AccessManager::AccessManager(const Identity& identity, const Name& dataset,
                             KeyChain& keyChain, Face& face)
  : m_identity(identity)
  , m_keyChain(keyChain)
  , m_face(face)
  , m_db(":memory:")
  , m_params(security::v2::KeyChain::getDefaultKeyParams())
{
  // NAC Identity: <identity>/NAC/<dataset>
  // generate NAC key
  auto nacId = m_keyChain.createIdentity(Name(identity.getName()).append(NAC).append(dataset), RsaKeyParams());
  m_nacKey = nacId.getDefaultKey();
  if (m_nacKey.getKeyType() != KeyType::RSA) {
    NDN_LOG_INFO("Cannot re-use existing KEK/KDK pair, as it is not an RSA key, regenerating");
    m_nacKey = m_keyChain.createKey(nacId, RsaKeyParams());
  }
  auto nacKeyId = m_nacKey.getName().at(-1);

  auto kekPrefix = Name(m_nacKey.getIdentity()).append(KEK);

  auto kek = make_shared<Data>(m_nacKey.getDefaultCertificate());
  kek->setName(Name(kekPrefix).append(nacKeyId));
  kek->setFreshnessPeriod(DEFAULT_KEK_FRESHNESS_PERIOD);
  m_keyChain.sign(*kek, signingByIdentity(m_identity));
  // kek looks like a cert, but doesn't have ValidityPeriod
  m_ims.insert(*kek);

  auto serveFromIms = [this] (const Name&, const Interest& interest) {
    auto data = m_ims.find(interest);
    if (data != nullptr) {
      NDN_LOG_DEBUG("Serving " << data->getName() << " from InMemoryStorage");
      m_face.put(*data);
    }
    else {
      NDN_LOG_DEBUG("Didn't find data for " << interest.getName());
      // send NACK?
    }
  };

  auto handleError = [] (const Name& prefix, const std::string& msg) {
    NDN_LOG_ERROR("Failed to register prefix " << prefix << ": " << msg);
  };

  m_kekReg = m_face.setInterestFilter(kekPrefix, serveFromIms, handleError);

  auto kdkPrefix = Name(m_nacKey.getIdentity()).append(KDK).append(nacKeyId);
  m_kdkReg = m_face.setInterestFilter(kdkPrefix, serveFromIms, handleError);
}

AccessManager::AccessManager(const Identity& identity, const Name& dataset,
                             KeyChain& keyChain, Face& face,
                             const std::string& dbPath, 
                             const KeyParams& params, const int freshPeriod)
  : m_identity(identity)
  , m_keyChain(keyChain)
  , m_face(face)
  , m_db(dbPath)
  , m_params(params)
  , m_freshPeriod(freshPeriod)
{
  // NAC Identity: <identity>/NAC/<dataset>
  // generate NAC key
  auto nacId = m_keyChain.createIdentity(Name(identity.getName()).append("READ"), RsaKeyParams());
  m_nacKey = nacId.getDefaultKey();
  if (m_nacKey.getKeyType() != KeyType::RSA) {
    NDN_LOG_INFO("Cannot re-use existing KEK/KDK pair, as it is not an RSA key, regenerating");
    m_nacKey = m_keyChain.createKey(nacId, RsaKeyParams());
  }

  m_last = false;
  m_found = false;

}

void
AccessManager::getKdk()
{
  std::cout << "Start listening...  " << std::endl;
  time::nanoseconds ts = time::toUnixTimestamp(time::system_clock::now());
  auto serveFromImsForKdk = [this] (const Name&, const Interest& interest) {
    auto data = m_ims.find(interest);
    if (data != nullptr) {
      NDN_LOG_DEBUG("Serving KDK:  " << data->getName() << " from InMemoryStorage");
      std::cout << "Manager Serving KDK: " << data->getName()  << " from InMemoryStorage" << std::endl;
      m_face.put(*data);
    }
    else {
      NDN_LOG_DEBUG("Didn't find data for " << interest.getName());
      // send NACK?
    }
  };

  auto handleError = [] (const Name& prefix, const std::string& msg) {
    NDN_LOG_ERROR("Failed to register prefix " << prefix << ": " << msg);
  };

  auto kdkPrefix = Name(m_nacKey.getIdentity()).append(KDK);
  m_kdkReg = m_face.setInterestFilter(kdkPrefix, serveFromImsForKdk, handleError);//listen to interest for KDK data for TNAC

  time::nanoseconds te = time::toUnixTimestamp(time::system_clock::now());
  std::cout << "KDK publishing time: " <<(te - ts)<< std::endl;
  
}

std::list<Data>
AccessManager::getKeklist()
{
  return m_dataList;
}

void
AccessManager::getGroupKey(const TimeStamp& stimeslot, const TimeStamp& etimeslot, const std::vector< std::vector<double>> locations, bool needRegenerate)
{
  std::map<Name, Buffer> memberKeysTime;
  std::map<Name, Buffer> memberKeysLocation;
  std::map<Name, Buffer> memberKeysTLocation;

  std::map<Name, Buffer> memberKeys;
  std::list<Data> result;

  m_startTimeSlot = stimeslot;
  m_endTimeSlot = etimeslot;

  size_t r = locations.size();

  if (stimeslot.is_not_a_date_time() && r) {

     std::cout << "only have location, no time  " << std::endl;
      for(size_t i = 0; i < r; i++)
      {

        m_found = false;
        // get location
        Location finalLocation = calculateLocation(locations[i][0], locations[i][1], locations[i][2], memberKeysLocation);

        Name kekPrefix = Name(m_nacKey.getIdentity()).append(KEK);
        Name kdkPrefix = Name(m_nacKey.getIdentity()).append(KDK);

          std::string centerLat ;
          std::string centerLon ;
          std::string rad ;

        if (m_found) {
          centerLat = to_string(finalLocation.getLatitude());
          centerLon = to_string(finalLocation.getLongitude());
          rad = to_string(finalLocation.getRadius());
          // Name kekPrefix = Name(m_nacKey.getIdentity()).append(KEK_Location);

        } 
        else {
          centerLat = to_string(locations[i][0]);
          centerLon = to_string(locations[i][1]);
          rad = to_string(locations[i][2]);

        }       
          std::cout << "Manager calculates location circle: " << centerLat << "  " << centerLon << "  " << rad << std::endl;   
        
          Name kekName = Name(kekPrefix).append("*").append("*").append(centerLat).append(centerLon).append(rad);

          // generate new key pair for the interval
          Certificate newCert;
          auto newIdentity = m_keyChain.createIdentity(kekName, m_params);
          newCert = newIdentity.getDefaultKey().getDefaultCertificate();

          auto keyId = newIdentity.getDefaultKey().getName().at(-1);

          // add the first element to the result
          // KEK (public key) data packet name convention:
          // /<data_type>/KEK/[start-ts]/[end-ts]
          Data data = createKekData(kekName.append(keyId), newCert);
          result.push_back(data);
          m_dataList.push_back(data);
          std::cout << "Manager generates KEK data: \n" << data << std::endl;

          // Name kdkPrefix = Name(m_nacKey.getIdentity()).append(KDK_Location);
          Name kdkName = kdkPrefix.append("*")
                                  .append("*")
                                  .append(centerLat)
                                  .append(centerLon)
                                  .append(rad)
                                  .append(keyId); // key-id

          // encrypt pri key with pub key from consumers' certificate
          for (const auto& entry : memberKeysLocation) {
            const Name& keyName = entry.first;
            const Buffer& certKey = entry.second;

            // generate the name of the packet
            // KDK (private key) data packet name convention:
            // /<data_type>/KDK/[start-ts]/[end-ts]/Encrypted-by/[member-name]
            data = createKdkData(kdkName, newCert, keyName, certKey);
            result.push_back(data);
            m_dataList.push_back(data);
            std::cout << "Manager generates KDK data: \n" << data << std::endl;
          }
        }

  }
  else if (r == 0) {
    std::cout << "only have time, no location  " << std::endl;

    while(!m_last){
    // get time interval
      Interval finalInterval = calculateInterval(m_startTimeSlot, memberKeysTime);

      Name kekPrefix = Name(m_nacKey.getIdentity()).append(KEK);
      Name kdkPrefix = Name(m_nacKey.getIdentity()).append(KDK);


      std::string startTime = boost::posix_time::to_iso_string(finalInterval.getStartTime());
      std::string endTime = boost::posix_time::to_iso_string(finalInterval.getEndTime());
      std::cout << "Manager calculates time interval: " << startTime << " - " << endTime << std::endl;

      if (boost::posix_time::from_iso_string(endTime) >= m_endTimeSlot) {
        m_last = true;
      }

      m_startTimeSlot = boost::posix_time::from_iso_string(endTime) + boost::posix_time::minutes(1);

      // Name kekPrefix = Name(m_nacKey.getIdentity()).append(KEK_Time);
      Name kekName = Name(kekPrefix).append(startTime).append(endTime).append("*").append("*").append("*");

      // generate new key pair for the interval
      Certificate newCert;
      auto newIdentity = m_keyChain.createIdentity(kekName, m_params);
      newCert = newIdentity.getDefaultKey().getDefaultCertificate();

      auto keyId = newIdentity.getDefaultKey().getName().at(-1);

      // add the first element to the result
      // KEK (public key) data packet name convention:
      // /<data_type>/KEK/[start-ts]/[end-ts]
      Data data = createKekData(kekName.append(keyId), newCert);
      result.push_back(data);
      m_dataList.push_back(data);
      std::cout << "Manager generates KEK data: \n" << data << std::endl;

      Name kdkName = kdkPrefix.append(startTime)
                              .append(endTime)
                              .append("*").append("*").append("*")
                              .append(keyId); // key-id

      // encrypt pri key with pub key from consumers' certificate
      for (const auto& entry : memberKeysTime) {
        const Name& keyName = entry.first;
        const Buffer& certKey = entry.second;

        // generate the name of the packet
        // KDK (private key) data packet name convention:
        // /<data_type>/KDK/[start-ts]/[end-ts]/Encrypted-by/[member-name]
        data = createKdkData(kdkName, newCert, keyName, certKey);
        result.push_back(data);
        m_dataList.push_back(data);
        std::cout << "Manager generates KDK data: \n" << data << std::endl;
      }
      // return result;
    }
  }
  else {
    std::cout << "have location and have time  " << std::endl;
    while(!m_last){
      std::string startTime;
      std::string endTime;
      for(size_t i = 0; i < r; i++)
      {
        // get time and location
        TimeLocation finalTimeLocation = calculateTimeLocation(m_startTimeSlot, locations[i][0], locations[i][1], locations[i][2], memberKeysTLocation);

        Name kekPrefix = Name(m_nacKey.getIdentity()).append(KEK);
        Name kdkPrefix = Name(m_nacKey.getIdentity()).append(KDK);

        startTime = boost::posix_time::to_iso_string(finalTimeLocation.getStartTime());
        endTime = boost::posix_time::to_iso_string(finalTimeLocation.getEndTime());
        std::cout << "Manager calculates time interval: " << startTime << " - " << endTime << std::endl;

        std::string centerLat = to_string(finalTimeLocation.getLatitude());
        std::string centerLon = to_string(finalTimeLocation.getLongitude());
        std::string rad = to_string(finalTimeLocation.getRadius());

        std::cout << "Manager calculates location circle: " << centerLat << "  " << centerLon << "  " << rad << std::endl;
        Name kekName = Name(kekPrefix).append(startTime).append(endTime).append(centerLat).append(centerLon).append(rad);

        // generate new key pair for the interval
        Certificate newCert;
        auto newIdentity = m_keyChain.createIdentity(kekName, m_params);
        newCert = newIdentity.getDefaultKey().getDefaultCertificate();

        auto keyId = newIdentity.getDefaultKey().getName().at(-1);

        // add the first element to the result
        // KEK (public key) data packet name convention:
        // /<data_type>/KEK/[start-ts]/[end-ts]
        Data data = createKekData(kekName.append(keyId), newCert);
        result.push_back(data);
        m_dataList.push_back(data);
        std::cout << "Manager generates KEK data: \n" << data << std::endl;

        // Name kdkPrefix = Name(m_nacKey.getIdentity()).append(KDK_Time_Location);
        Name kdkName = kdkPrefix.append(startTime)
                                .append(endTime)
                                .append(centerLat)
                                .append(centerLon)
                                .append(rad)
                                .append(keyId); // key-id

          // encrypt pri key with pub key from consumers' certificate
          for (const auto& entry : memberKeysTLocation) {
            const Name& keyName = entry.first;
            const Buffer& certKey = entry.second;

            // generate the name of the packet
            // KDK (private key) data packet name convention:
            // /<data_type>/KDK/[start-ts]/[end-ts]/Encrypted-by/[member-name]
            data = createKdkData(kdkName, newCert, keyName, certKey);
            result.push_back(data);
            m_dataList.push_back(data);
            std::cout << "Manager generates KDK data: \n" << data << std::endl;
          }
      }

      if (boost::posix_time::from_iso_string(endTime) >= m_endTimeSlot) {
        m_last = true;
      }
      m_startTimeSlot = boost::posix_time::from_iso_string(endTime) + boost::posix_time::minutes(1);
    }
    
  }
}

Data
AccessManager::addMember(const Certificate& memberCert)
{
  Name kdkName(m_nacKey.getIdentity());
  kdkName
    .append(KDK)
    .append(m_nacKey.getName().at(-1)) // key-id
    .append(ENCRYPTED_BY)
    .append(memberCert.getKeyName());

  const size_t secretLength = 32;
  uint8_t secret[secretLength + 1];
  random::generateSecureBytes(secret, secretLength);
  // because of stupid bug in ndn-cxx, remove all \0 in generated secret, replace with 1
  for (size_t i = 0; i < secretLength; ++i) {
    if (secret[i] == 0) {
      secret[i] = 1;
    }
  }
  secret[secretLength] = 0;

  auto kdkData = m_keyChain.exportSafeBag(m_nacKey.getDefaultCertificate(),
                                          reinterpret_cast<const char*>(secret), secretLength);

  PublicKey memberKey;
  memberKey.loadPkcs8(memberCert.getPublicKey().data(), memberCert.getPublicKey().size());

  EncryptedContent content;
  content.setPayload(kdkData->wireEncode());
  content.setPayloadKey(memberKey.encrypt(secret, secretLength));

  auto kdk = make_shared<Data>(kdkName);
  kdk->setContent(content.wireEncode());
  // FreshnessPeriod can serve as a soft access control for revoking access
  kdk->setFreshnessPeriod(DEFAULT_KDK_FRESHNESS_PERIOD);
  m_keyChain.sign(*kdk, signingByIdentity(m_identity));

  m_ims.insert(*kdk);

  return *kdk;
}

void
AccessManager::addMember(const std::string& scheduleName, const Certificate& cert)
{
  Buffer keybits = cert.getPublicKey();
  m_db.addMember(scheduleName, cert.getKeyName(), keybits);
}

void
AccessManager::addMemberLocation(const std::string& listName, const Certificate& memberCert)
{
  Buffer keybits = memberCert.getPublicKey();
  m_db.addMemberLocation(listName, memberCert.getKeyName(), keybits);
}

void
AccessManager::addMemberTimeLocation(const std::string& listName, const Certificate& memberCert)
{
  Buffer keybits = memberCert.getPublicKey();
  m_db.addMemberTimeLocation(listName, memberCert.getKeyName(), keybits);
}

void
AccessManager::addSchedule(const std::string& scheduleName, const Schedule& schedule)
{
  m_db.addSchedule(scheduleName, schedule);
}

void
AccessManager::addLocationList(const std::string& name, const LocationList& locationlist)
{
  m_db.addLocationList(name, locationlist);
}

void
AccessManager::addTimeLocationList(const std::string& name, const TimeLocationList& locationlist)
{
  m_db.addTimeLocationList(name, locationlist);
}

Data
AccessManager::createKekData(const Name& kekName,
                             const Certificate& newCert)
{
  auto kek = make_shared<Data>(newCert);
  kek->setName(kekName);
  kek->setFreshnessPeriod(time::hours(m_freshPeriod));
  m_keyChain.sign(*kek, signingByIdentity(m_identity));
  
  m_ims.insert(*kek);
  return *kek;
}

Data
AccessManager::createKdkData(const Name& kdkPrefix,
                             const Certificate& newCert,
                             const Name& keyName,
                             const Buffer& certKey)
{
  Name kdkName = kdkPrefix;
  kdkName
      .append(ENCRYPTED_BY)
      .append(keyName);

  const size_t secretLength = 32;
  uint8_t secret[secretLength + 1];
  random::generateSecureBytes(secret, secretLength);
  // because of stupid bug in ndn-cxx, remove all \0 in generated secret, replace with 1
  for (size_t i = 0; i < secretLength; ++i) {
    if (secret[i] == 0) {
      secret[i] = 1;
    }
  }
  secret[secretLength] = 0;

  auto kdkData = m_keyChain.exportSafeBag(newCert, reinterpret_cast<const char*>(secret), secretLength);

  PublicKey memberKey;
  memberKey.loadPkcs8(certKey.data(), certKey.size());

  EncryptedContent content;
  content.setPayload(kdkData->wireEncode());
  content.setPayloadKey(memberKey.encrypt(secret, secretLength));

  auto kdk = make_shared<Data>();
  kdk->setName(kdkName);
  kdk->setContent(content.wireEncode());
  // FreshnessPeriod can serve as a soft access control for revoking access
  kdk->setFreshnessPeriod(time::hours(m_freshPeriod));
  m_keyChain.sign(*kdk, signingByIdentity(m_identity));

  m_ims.insert(*kdk);
  return *kdk;
}

void
AccessManager::removeMember(const Name& identity)
{
  m_ims.erase(Name(m_nacKey.getName()).append(KDK).append(ENCRYPTED_BY).append(identity));
}

Interval
AccessManager::calculateInterval(const TimeStamp& timeslot, std::map<Name, Buffer>& memberKeys)
{
  // prepare
  Interval positiveResult(false);
  Interval negativeResult(false);
  Interval tempInterval(false);
  Interval finalInterval(false);
  bool isPositive;
  memberKeys.clear();

  // get the all intervals from schedules
  for (const std::string& scheduleName : m_db.listAllScheduleNames()) {
    const Schedule& schedule = m_db.getSchedule(scheduleName);
    std::tie(isPositive, tempInterval) = schedule.getCoveringInterval(timeslot);

    std::string startTime = boost::posix_time::to_iso_string(tempInterval.getStartTime());
    std::string endTime = boost::posix_time::to_iso_string(tempInterval.getEndTime());

    if (isPositive) {
      if (!positiveResult.isValid())
        positiveResult = tempInterval;
      positiveResult&& tempInterval;

      std::map<Name, Buffer> m = m_db.getScheduleMembers(scheduleName);
      memberKeys.insert(m.begin(), m.end());
    }
    else {
      if (!negativeResult.isValid())
        negativeResult = tempInterval;
      negativeResult&& tempInterval;
    }

  }

  if (!positiveResult.isValid()) {
    // return invalid interval when there is no member has interval covering the
    // time slot
    return negativeResult;
  }

  // get the final interval result
  if (negativeResult.isValid())
    finalInterval = positiveResult && negativeResult;
  else
    finalInterval = positiveResult;

  return finalInterval;
}

Location
AccessManager::calculateLocation(const double& latitude, const double& longitude, const double& radius, std::map<Name, Buffer>& memberKeys)
{
  Location positiveLocation;
  Location negativeLocation;
  Location finalLocation;
  Location tempLocation;

  bool isPositive;
  memberKeys.clear();

  // get all locations from list
  for (const std::string& locationName : m_db.listAllLocationListNames("LocationList")) {

    const LocationList& locationList = m_db.getLocationList(locationName);
    std::tie(isPositive, tempLocation) = locationList.getCoveringLocation(latitude, longitude, radius);

    if (isPositive) {
      m_found = true;
      if (!positiveLocation.isValid())
        positiveLocation = tempLocation;
      finalLocation = tempLocation;
      std::map<Name, Buffer> m = m_db.getScheduleMembersL(locationName);
      memberKeys.insert(m.begin(), m.end());
    }
    else {
      if (!negativeLocation.isValid())
        negativeLocation = tempLocation;
    }
  }

  if (positiveLocation.isValid()) {
    return  finalLocation;
  }

  // get the final interval result
  if (negativeLocation.isValid())
    finalLocation = negativeLocation;

  return Location(false);
}

TimeLocation
AccessManager::calculateTimeLocation(const TimeStamp& timeslot, const double& latitude, const double& longitude, const double& radius, std::map<Name, Buffer>& memberKeys)
{
  TimeLocation positiveResult;
  TimeLocation negativeResult;
  
  TimeLocation finalLocation;
  TimeLocation tempLocation;
  
  bool isPositive;
  memberKeys.clear();

  // get the all locations from list
  for (const std::string& locationName : m_db.listAllLocationListNames("TimeLocationList")) {
    const TimeLocationList& locationList = m_db.getTimeLocationList(locationName);

    std::tie(isPositive, tempLocation) = locationList.getCoveringTimeLocation(timeslot, latitude, longitude, radius);
      
    std::string startTime = boost::posix_time::to_iso_string(tempLocation.getStartTime());
    std::string endTime = boost::posix_time::to_iso_string(tempLocation.getEndTime());

    if (isPositive) {
      if (!positiveResult.isValid())
        positiveResult = tempLocation;
      positiveResult&& tempLocation;

      std::map<Name, Buffer> m = m_db.getScheduleMembersL(locationName);
      memberKeys.insert(m.begin(), m.end());

    }
    else {
      if (!negativeResult.isValid())
        negativeResult = tempLocation;
      negativeResult&& tempLocation;
    }
  }

  if (!positiveResult.isValid()) {
    // return invalid interval when there is no member has interval covering the
    // time slot
    return negativeResult;
  }

  // get the final interval result
  if (negativeResult.isValid())
    finalLocation = positiveResult && negativeResult;
  else
    finalLocation = positiveResult;
  return finalLocation;
}

} // namespace nac
} // namespace ndn
