# NAC: Named-Based Access Control

[![Build Status](https://travis-ci.org/named-data/name-based-access-control.svg?branch=new)](https://travis-ci.org/named-data/name-based-access-control)

Please submit any bugs or issues to the NAC issue tracker:
https://redmine.named-data.net/projects/nac/issues
