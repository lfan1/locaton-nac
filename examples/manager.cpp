/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/**
 * Copyright (c) 2014-2018, Regents of the University of California
 *
 * NAC library is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * NAC library is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.
 *
 * You should have received copies of the GNU General Public License and GNU Lesser
 * General Public License along with ndn-cxx, e.g., in COPYING.md file.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * See AUTHORS.md for complete list of NAC library authors and contributors.
 */

#include <ndn-cxx/face.hpp>
#include <ndn-cxx/security/key-chain.hpp>
#include <ndn-cxx/security/validator-config.hpp>

#include "encryptor.hpp"
#include "access-manager.hpp"

#include <iostream>
#include <boost/filesystem.hpp>
#include "ndn-cxx/util/io.hpp"

// Enclosing code in ndn simplifies coding (can also use `using namespace ndn`)
namespace ndn {
namespace nac {
// Additional nested namespaces can be used to prevent/limit name conflicts
namespace examples {


static RsaKeyParams params;

using namespace boost::posix_time;
using namespace std;

class Manager : noncopyable
{
public:
  Manager()
    : m_face(nullptr, m_keyChain)
    , m_validator(m_face)
    , m_accessManager(m_keyChain.createIdentity("/org/md2k", RsaKeyParams()), "test",
                      m_keyChain, m_face,
                      "/Users/laqinfan/data/data.db",
                      params, 1)
  {
    m_validator.load(R"CONF(
        trust-anchor
        {
          type any
        }
      )CONF", "fake-config");
  }

  inline security::v2::Certificate
  loadCertificate(const std::string& fileName)
  {
    shared_ptr<security::v2::Certificate> cert;
    cert = io::load<security::v2::Certificate>(fileName);

    if (cert == nullptr) {
      BOOST_THROW_EXCEPTION(std::runtime_error("Cannot load certificate from " + fileName));
    }
    return *cert;
  }

  vector<string> 
  split(string str, char delimiter) {
    vector<string> internal;
    stringstream ss(str); // Turn the string into a stream.
    string tok;

    while(getline(ss, tok, delimiter)) {
        internal.push_back(tok);
    }

    return internal;
  }

  void
  nac_add_member(const std::string& scheduleMember)
  {
    std::string schedule;
    std::string member;

    vector<string> sep = split(scheduleMember, ' ');
    std::cout << "schedule member: "<< sep[0] <<" " << sep[1]<< std::endl;
    schedule = sep[0];
    member = sep[1];

    try {
        auto cert = loadCertificate(member);
        std::cout << "cert: "<< cert.getKeyName() << std::endl; 
        std::ifstream file(schedule);
        std::string str;
        Schedule s;

        while (std::getline(file, str)) {
            vector<string> sep1 = split(str, ' ');
            std::string s1 = sep1[0];
            std::string s2 = sep1[1];
            size_t s3;
            std::stringstream sstream1(sep1[2]);
            sstream1 >> s3;
            size_t s4;
            std::stringstream sstream2(sep1[3]);
            sstream2 >> s4;
            size_t s5;
            std::stringstream sstream3(sep1[4]);
            sstream3 >> s5;
            std::string s6 = sep1[5];
            std::string s7 = sep1[6];

            Interval::RepeatUnit unit;

            if (s6 == "DAY") {
                unit = Interval::RepeatUnit::DAY;

            } else if (s6 == "MONTH") {
                unit = Interval::RepeatUnit::MONTH;

            } else if (s6 == "YEAR") {
                unit = Interval::RepeatUnit::YEAR;

            } else {
                unit = Interval::RepeatUnit::NONE;

            }

            Interval interval(from_iso_string(s1),
                                        from_iso_string(s2),
                                        s3,
                                        s4,
                                        s5,
                                        unit);

            if (s7 == "Allow") {
                s.addWhiteInterval(interval);
            } else {
                s.addBlackInterval(interval);
            }   
        }

        std::string scheduleName = boost::filesystem::path(schedule).stem().string();
        // add to the group manager db
        m_accessManager.addSchedule(scheduleName, s);

        // add members to the database
        m_accessManager.addMember(scheduleName, cert);
    }
    catch (const std::exception& e) {
        std::cerr << "ERROR: " << e.what() << std::endl;
    }

  }

  void
  nac_add_member_location(const std::string& scheduleMember)
  {
    std::string locationlist;
    std::string member;

    vector<string> sep = split(scheduleMember, ' ');
    std::cout << "locationlist and member: "<< sep[0] << " " << sep[1]<< std::endl;
    locationlist = sep[0];
    member = sep[1];

    try {
        auto cert = loadCertificate(member);

        // cert.setName(Name("/ndn/memberB/KEY/").appendVersion().append("/123")); //for demo on the same computer

        std::cout << "cert: "<< cert.getName() << std::endl; 
        std::ifstream file(locationlist);
        std::string str;
        LocationList l;

        while (std::getline(file, str)) {
            vector<string> sep1 = split(str, ' ');
            std::string s1 = sep1[0];
            std::string s2 = sep1[1];
            std::string s3 = sep1[2];
            std::string s4 = sep1[3];

            Location location(std::stod(s1), std::stod(s2), std::stod(s3));

            if (s4 == "Allow") {
                l.addWhiteLocation(location);
            } else {
                l.addBlackLocation(location);
            }   
        }

        std::string scheduleName = boost::filesystem::path(locationlist).stem().string();
        // add to the group manager db
        m_accessManager.addLocationList(scheduleName, l);

        // add members to the database
        m_accessManager.addMemberLocation(scheduleName, cert);
    }
    catch (const std::exception& e) {
        std::cerr << "ERROR: " << e.what() << std::endl;
    }

  }

  void
  nac_add_member_location_time(const std::string& scheduleMember)
  {
    std::string schedule;
    std::string member;

    vector<string> sep = split(scheduleMember, ' ');
    std::cout << "access-policy and member: "<< sep[0] << " " << sep[1]<< std::endl;
    schedule = sep[0];
    member = sep[1];

    try {
        auto cert = loadCertificate(member);

        // cert.setName(Name("/ndn/memberB/KEY/").appendVersion().append("/123")); //for demo on the same computer

        std::cout << "cert: "<< cert.getName() << std::endl; 
        std::ifstream file(schedule);
        std::string str;

        TimeLocationList l;

        while (std::getline(file, str)) {
          
            vector<string> sep1 = split(str, ' ');
            std::string s1 = sep1[0];
            std::string s2 = sep1[1];
            size_t s3;
            std::stringstream sstream1(sep1[2]);
            sstream1 >> s3;
            size_t s4;
            std::stringstream sstream2(sep1[3]);
            sstream2 >> s4;
            size_t s5;
            std::stringstream sstream3(sep1[4]);
            sstream3 >> s5;
            std::string s6 = sep1[5];
    
            std::string s7 = sep1[6];
            std::string s8 = sep1[7];
            std::string s9 = sep1[8];
            std::string s10 = sep1[9];

            TimeLocation::RepeatUnit unit;
            if (s6 == "DAY") {
                unit = TimeLocation::RepeatUnit::DAY;

            } else if (s6 == "MONTH") {
                unit = TimeLocation::RepeatUnit::MONTH;

            } else if (s6 == "YEAR") {
                unit = TimeLocation::RepeatUnit::YEAR;

            } else {
                unit = TimeLocation::RepeatUnit::NONE;

            }
            
            TimeLocation location(std::stod(s7), 
                                  std::stod(s8), 
                                  std::stod(s9), 
                                  from_iso_string(s1),
                                  from_iso_string(s2),
                                  s3,
                                  s4,
                                  s5,
                                  unit);

            if (s10 == "Allow") {
                l.addWhiteTimeLocation(location);
            } else {
                l.addBlackTimeLocation(location);
            }   
        }

        std::string scheduleName = boost::filesystem::path(schedule).stem().string();

         // add timelocationlist to db
        m_accessManager.addTimeLocationList(scheduleName, l);

        m_accessManager.addMemberTimeLocation(scheduleName, cert);

    }
    catch (const std::exception& e) {
        std::cerr << "ERROR: " << e.what() << std::endl;
    }

  }

  void
  run()
  {
    // give access to default identity. If consumer uses the same default identity, he will be able to decrypt
    // auto cert = m_keyChain.getPib().getDefaultIdentity().getDefaultKey().getDefaultCertificate();

    // configure start-time and end-time for policies
    std::string starttime = "20200525T000100";
    std::string endTime = "20200529T000000";
    TimeStamp sTimeslot = boost::posix_time::from_iso_string(starttime);
    TimeStamp eTimeslot = boost::posix_time::from_iso_string(endTime);
    // TimeStamp sTimeslot;

    // configure location list
    std::vector< std::vector<double>>  locations;
    locations = {{35.120862, -89.936092, 100},{35.121185, -89.938107, 60}};
    time::nanoseconds t_start = time::toUnixTimestamp(time::system_clock::now());
    m_accessManager.getGroupKey(sTimeslot, eTimeslot, locations);

    std::cout <<"key size: " << m_accessManager.getKeklist().size()<< std::endl;
    time::nanoseconds t_end = time::toUnixTimestamp(time::system_clock::now());
		std::cout << "KEK/KDK generarion time: " <<(t_end - t_start)<< std::endl;
  
    m_face.setInterestFilter("/org/md2k/READ/KEK",
                             bind(&Manager::onInterest, this, _1, _2),
                             RegisterPrefixSuccessCallback(),
                             bind(&Manager::onRegisterFailed, this, _1, _2));

    m_accessManager.getKdk();

    time::nanoseconds t_end1 = time::toUnixTimestamp(time::system_clock::now());
	  std::cout << "KEK/KDK publishing time: " <<(t_end1 - t_end)<< std::endl;

    m_face.processEvents();
  }

private:
  void
  onInterest(const InterestFilter& filter, const Interest& interest)
  {
    time::nanoseconds ts = time::toUnixTimestamp(time::system_clock::now());
    std::cout <<"\n" << std::endl;
    std::cout << "Manager generate KekData for time-location nac: " << interest.getName() << ", need to generate one!" << std::endl;
    
    std::string time = interest.getName().get(-4).toUri();
    std::string lat = interest.getName().get(-3).toUri();
    std::string lon = interest.getName().get(-2).toUri();
    std::string rad = interest.getName().get(-1).toUri();

    time::system_clock::TimePoint timeslot;
    timeslot = time::fromIsoString(time);

    std::list<Data> keys =  m_accessManager.getKeklist();

    std::list<Data>::iterator it;
    for (it = keys.begin(); it != keys.end(); ++it) {

      Name keyName = it->getName();

      if ( lat == keyName.get(-4).toUri() && lon == keyName.get(-3).toUri() && rad == keyName.get(-2).toUri()
            && timeslot >= time::fromIsoString(keyName.get(-6).toUri())&& timeslot < time::fromIsoString(keyName.get(-5).toUri()))
        {
          shared_ptr<Data> ekey = make_shared<Data>(*it);

          Name subName = keyName.getSubName(Name(m_accessManager.m_nacKey.getIdentity()).size());
          Name interestName = interest.getName();

          Name dataName = interestName.append(subName);
          // Name keyName = dataName.append(KEK_Time_Location).append(name.get(-6)).append(name.get(-5)).append(name.get(-4)).append(name.get(-3)).append(name.get(-2)).append(name.get(-1));
          ekey->setName(dataName);
          m_face.put(*ekey);
          std::cout << "Manager sends out KEK data for TLNAC: " << keyName << std::endl;
          std::cout <<"\n" << std::endl;

          break;
        } 
    }

    time::nanoseconds te = time::toUnixTimestamp(time::system_clock::now());
    std::cout << "KEK publishing time: " <<(te - ts)<< std::endl;
  }

  void
  onRegisterFailed(const Name& prefix, const std::string& reason)
  {
    std::cerr << "ERROR: Failed to register prefix \""
              << prefix << "\" in local hub's daemon (" << reason << ")"
              << std::endl;
    m_face.shutdown();
  }

private:
  KeyChain m_keyChain;
  Face m_face;
  ValidatorConfig m_validator;
  AccessManager m_accessManager;
};

} // namespace examples
} // namespace nac
} // namespace ndn

std::string nac_helper = R"STR(
  help          Show all commands
  version       Show version and exit
  run           run directly
  add-member    Add schedule to the member
)STR";

int
main(int argc, char** argv)
{
  if (argc < 2) {
    std::cerr << nac_helper << std::endl;
    return 1;
  }

  ndn::nac::examples::Manager manager;

  using namespace ndn::nac;

  std::string command(argv[1]);
  try {
    if (command == "help")              { std::cout << nac_helper << std::endl; }
    else if (command == "version")      { std::cout << "Version" << std::endl; }
    else if (command == "run")   {
        std::cout << "directly run" << std::endl;
        try {
            manager.run();
        }
        catch (const std::exception& e) {
            std::cerr << "ERROR: " << e.what() << std::endl;
        }
        return 0;
        }
    else if (command == "add-member") {

        std::string scheduleMember;

        std::cout << "Enter time-shcedule and member: "<< std::endl;
        while(getline(std::cin, scheduleMember)&&scheduleMember.length() > 0){
            manager.nac_add_member(scheduleMember); 
        }

        std::cout << "Enter location-schedule and member: "<< std::endl;
        while(getline(std::cin, scheduleMember)&&scheduleMember.length() > 0){
            manager.nac_add_member_location(scheduleMember); 
        }

        std::cout << "Enter time-location-schedule and member: "<< std::endl;
        while(getline(std::cin, scheduleMember)&&scheduleMember.length() > 0){
            manager.nac_add_member_location_time(scheduleMember); 
        }

        std::cout <<"Set manager successfully!" << std::endl;
        // return producer.nac_add_member(argc - 1, argv + 1); 
        try {
            manager.run();
        }
        catch (const std::exception& e) {
            std::cerr << "ERROR: " << e.what() << std::endl;
        }
        return 0;

    }
    else {
      std::cerr << nac_helper << std::endl;
      return 1;
    }
  }
  catch (const std::exception& e) {
    std::cerr << "ERROR: " << e.what();
    return 1;
  }
}
